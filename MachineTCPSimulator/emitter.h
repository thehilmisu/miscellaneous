#ifndef EMITTER_H
#define EMITTER_H

#include <QObject>

class Emitter : public QObject
{
    Q_OBJECT
public:
    explicit Emitter(QObject *parent = 0);
    
    void emitSignal(int i)
    {
        emit ac(i);
    }

signals:
    void ac(int conn);
    
public slots:
    
};

#endif // EMITTER_H
