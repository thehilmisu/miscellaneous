#-------------------------------------------------
#
# Project created by QtCreator 2019-09-30T14:29:32
#
#-------------------------------------------------

QT       += core gui network

TARGET = MachineTCPSimulator
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    linkastcpclient.cpp \
    emitter.cpp

HEADERS  += mainwindow.h \
    definitions.h \
    linkastcpclient.h \
    emitter.h

FORMS    += mainwindow.ui
