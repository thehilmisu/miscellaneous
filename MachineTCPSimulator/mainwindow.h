#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include "linkastcpclient.h"
#include "emitter.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    Emitter *emitter;
    ~MainWindow();


public slots:
    void connect2server();
    void startSimulation();
    void testServerConnected();
    void testServerDisconnected();
    void activeConnectionStatus(int conn);
    void timertimeout();
    
private:
    Ui::MainWindow *ui;
    QList<LinkasTCPClient *> client;
    QTimer *timer;
    void testConnection();
    QTcpSocket testSocket;
    bool connected;
    int numberOfConnections;

};

#endif // MAINWINDOW_H
