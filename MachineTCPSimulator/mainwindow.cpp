#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    client.clear();

    connect(ui->btnConnect,SIGNAL(clicked()),this,SLOT(connect2server()));
    connect(ui->btnStartSimulation,SIGNAL(clicked()),this,SLOT(startSimulation()));

    ui->btnStartSimulation->setEnabled(false);

    connected = false;
    numberOfConnections = 0;

    emitter = new Emitter();
    connect(emitter,SIGNAL(ac(int)),this,SLOT(activeConnectionStatus(int)));

    timer = new QTimer();
    timer->setInterval(1000);
    connect(timer,SIGNAL(timeout()),this,SLOT(timertimeout()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::timertimeout()
{
    timer->stop();

    for(int i=0;i<client.size();i++)
    {
        if(client.at(i)->getSocket()->isOpen())
            numberOfConnections++;
        else
            numberOfConnections--;

        if(numberOfConnections<0)
            numberOfConnections = 0;
    }

    ui->lblActiveConnectionCount->setText("Active connection count: "+QString::number(numberOfConnections));

    timer->start();
}

void MainWindow::connect2server()
{
    if(!connected)
    {
        testConnection();
    }
}

void MainWindow::testConnection()
{
    testSocket.connectToHost(QHostAddress(ui->txtServer->text()),ui->txtPort->text().toInt());
    connect(&testSocket,SIGNAL(connected()),this,SLOT(testServerConnected()));
    connect(&testSocket,SIGNAL(disconnected()),this,SLOT(testServerDisconnected()));
}

void MainWindow::testServerConnected()
{
    connected = true;
    qDebug() << "test socket successfully connected";
    ui->btnStartSimulation->setEnabled(true);
}
void MainWindow::testServerDisconnected()
{
    qDebug() << "test socket disconnected";
}

void MainWindow::activeConnectionStatus(int conn)
{

}

void MainWindow::startSimulation()
{
    qDebug() << "creating socket objects";
    QString tmpDeviceID;

    int i = 0;
    for(i=0;i<500;i++)
    {
        tmpDeviceID.clear();
        tmpDeviceID.sprintf("B%05d",i);
        //qDebug() << tmpDeviceID;
        client.append(new LinkasTCPClient(QHostAddress(ui->txtServer->text()),ui->txtPort->text().toInt(),tmpDeviceID,emitter));
        connect(client.at(i),SIGNAL(activeConnection(int)),this,SLOT(activeConnectionStatus(int)));
    }
}
