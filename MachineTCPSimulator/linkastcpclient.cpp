#include "linkastcpclient.h"

LinkasTCPClient::LinkasTCPClient(QHostAddress addr,int p,QString dID,Emitter *e)
{
    address = addr;
    port = p;
    connectionStatus = false;
    deviceID = dID;
    state = CONNECTION_STATE;

    socket = new QTcpSocket();
    connect(socket,SIGNAL(connected()),this,SLOT(socketConnected()));
    connect(socket,SIGNAL(disconnected()),this,SLOT(socketDisconnected()));

    em = e;

    periodic_list.append(PERIODIC_MESSAGE_EXC);
    periodic_list.append(PERIODIC_MESSAGE_EXC_2);
    periodic_list.append(MESSAGE_EXAMPLE_1);
    periodic_list.append(MESSAGE_EXAMPLE_2);
    periodic_list.append(MESSAGE_EXAMPLE_3);
    periodic_list.append(MESSAGE_EXAMPLE_4);
    periodic_list.append(MESSAGE_EXAMPLE_5);
    periodic_list.append(MESSAGE_EXAMPLE_6);
    periodic_list.append(MESSAGE_EXAMPLE_7);
    periodic_list.append(MESSAGE_EXAMPLE_8);
    periodic_list.append(MESSAGE_EXAMPLE_9);
    periodic_list.append(MESSAGE_EXAMPLE_10);


    this->start();

}

void LinkasTCPClient::socketConnected()
{
    //mute.lock();
    qDebug() << "conn";
    connectionStatus = true;
    //mute.unlock();
}

void LinkasTCPClient::socketDisconnected()
{
    //mute.lock();
    connectionStatus = false;
    //mute.unlock();
}

void LinkasTCPClient::run()
{
    forever
    {
        //try, connect, send
        if(state == CONNECTION_STATE)
        {
            socket->connectToHost(address,port);
            if(socket->isOpen())
            {
                //mute.lock();
                state = INTRODUCTION_STATE;
                //mute.unlock();
                this->sleep(5);
            }
        }
        else if(state == INTRODUCTION_STATE)
        {
            //mute.lock();
            QString tmp = deviceID+QString(INTRODUCTION_MESSAGE);
            //qDebug() << tmp;
            //socket->write((const char*)&size); //write size of data
            socket->write(tmp.toUtf8().data()); //write the data itself
            if(!socket->waitForBytesWritten())
            {
                state = CONNECTION_STATE;
            }
            else
            {
                state = PERIODIC_STATE;
            }

            //mute.unlock();
        }
        else if(state == PERIODIC_STATE)
        {
            static int periodic_type = 0;
            //mute.lock();
            if(periodic_type>periodic_list.size()-1)
                periodic_type = 0;

            QString tmp = deviceID+QString(periodic_list.at(periodic_type));

            socket->write(tmp.toUtf8().data()); //write the data itself

            qDebug() << tmp.toUtf8().data();

            if(!socket->waitForBytesWritten())
            {
                state = CONNECTION_STATE;
            }
            //mute.unlock();

            periodic_type++;

        }

        //qDebug() << deviceID << state;
        this->sleep(10);
    }
}
