/****************************************************************************
** Meta object code from reading C++ file 'smartlinktest.h'
**
** Created: Thu Aug 8 10:17:24 2019
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../smartlink_test/smartlinktest.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'smartlinktest.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_smartlinkTest[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      23,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x08,
      31,   14,   14,   14, 0x08,
      48,   14,   14,   14, 0x08,
      60,   14,   14,   14, 0x08,
      80,   14,   14,   14, 0x08,
     106,   14,   14,   14, 0x08,
     132,   14,   14,   14, 0x08,
     149,   14,   14,   14, 0x08,
     164,   14,   14,   14, 0x08,
     187,   14,   14,   14, 0x08,
     200,   14,   14,   14, 0x08,
     214,   14,   14,   14, 0x08,
     229,   14,   14,   14, 0x08,
     253,   14,   14,   14, 0x08,
     267,   14,   14,   14, 0x08,
     283,   14,   14,   14, 0x08,
     298,   14,   14,   14, 0x08,
     315,   14,   14,   14, 0x08,
     334,   14,   14,   14, 0x08,
     358,   14,   14,   14, 0x08,
     378,   14,   14,   14, 0x08,
     397,   14,   14,   14, 0x08,
     424,  411,   14,   14, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_smartlinkTest[] = {
    "smartlinkTest\0\0slot_openPort()\0"
    "slot_closePort()\0slot_send()\0"
    "slot_request_imei()\0slot_request_GSMversion()\0"
    "customMessageBox(QString)\0fillPortsCombo()\0"
    "checkSoftVer()\0slot_setSerialNumber()\0"
    "slot_setIP()\0slot_setIP2()\0slot_setPort()\0"
    "slot_readSerialNumber()\0slot_readIP()\0"
    "slot_readPort()\0slot_upDebug()\0"
    "slot_downDebug()\0slot_resetFields()\0"
    "slot_request_software()\0openParameterForm()\0"
    "slot_request_APN()\0slot_setAPN()\0"
    "dataReceived\0slot_newDataReceived(QChar)\0"
};

void smartlinkTest::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        smartlinkTest *_t = static_cast<smartlinkTest *>(_o);
        switch (_id) {
        case 0: _t->slot_openPort(); break;
        case 1: _t->slot_closePort(); break;
        case 2: _t->slot_send(); break;
        case 3: _t->slot_request_imei(); break;
        case 4: _t->slot_request_GSMversion(); break;
        case 5: _t->customMessageBox((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 6: _t->fillPortsCombo(); break;
        case 7: _t->checkSoftVer(); break;
        case 8: _t->slot_setSerialNumber(); break;
        case 9: _t->slot_setIP(); break;
        case 10: _t->slot_setIP2(); break;
        case 11: _t->slot_setPort(); break;
        case 12: _t->slot_readSerialNumber(); break;
        case 13: _t->slot_readIP(); break;
        case 14: _t->slot_readPort(); break;
        case 15: _t->slot_upDebug(); break;
        case 16: _t->slot_downDebug(); break;
        case 17: _t->slot_resetFields(); break;
        case 18: _t->slot_request_software(); break;
        case 19: _t->openParameterForm(); break;
        case 20: _t->slot_request_APN(); break;
        case 21: _t->slot_setAPN(); break;
        case 22: _t->slot_newDataReceived((*reinterpret_cast< QChar(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData smartlinkTest::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject smartlinkTest::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_smartlinkTest,
      qt_meta_data_smartlinkTest, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &smartlinkTest::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *smartlinkTest::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *smartlinkTest::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_smartlinkTest))
        return static_cast<void*>(const_cast< smartlinkTest*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int smartlinkTest::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 23)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 23;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
