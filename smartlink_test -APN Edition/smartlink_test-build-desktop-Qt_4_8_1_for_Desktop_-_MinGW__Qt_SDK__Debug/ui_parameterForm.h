/********************************************************************************
** Form generated from reading UI file 'parameterForm.ui'
**
** Created: Thu Aug 8 10:17:18 2019
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PARAMETERFORM_H
#define UI_PARAMETERFORM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_parameterForm
{
public:

    void setupUi(QWidget *parameterForm)
    {
        if (parameterForm->objectName().isEmpty())
            parameterForm->setObjectName(QString::fromUtf8("parameterForm"));
        parameterForm->setWindowModality(Qt::WindowModal);
        parameterForm->resize(572, 304);

        retranslateUi(parameterForm);

        QMetaObject::connectSlotsByName(parameterForm);
    } // setupUi

    void retranslateUi(QWidget *parameterForm)
    {
        parameterForm->setWindowTitle(QApplication::translate("parameterForm", "Form", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class parameterForm: public Ui_parameterForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PARAMETERFORM_H
