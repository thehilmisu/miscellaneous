#ifndef LINKASSERIALPORTLISTER_H
#define LINKASSERIALPORTLISTER_H

//********************************************************************************************
//version : 1.0.0
//  *initial release
//  *sadece windowsta derlenebilir hale getirilecek
//********************************************************************************************
#include <QList>
#include <QString>

#include <windows.h>
#include <setupapi.h>

struct QextPortInfo
{
    QString portName;
    QString physName;
    QString friendName;
    QString enumName;
};

class LinkasSerialPortLister
{
public:
    LinkasSerialPortLister();
    ~LinkasSerialPortLister();
    QList<QextPortInfo> getActivePortList(void);

private:
    QList<QextPortInfo>activePortList;

    void setupAPIScan(void);
    QString getRegKeyValue(HKEY key, LPCTSTR property);
    QString getDeviceProperty(HDEVINFO devInfo, PSP_DEVINFO_DATA devData, DWORD property);
};

#endif  //LINKASSERIALPORTLISTER_H
