#ifndef SMARTLINKTEST_H
#define SMARTLINKTEST_H

#include <QMainWindow>
#include "QMessageBox"
#include "SerialPort/LinkasSerialPortThread.h"
#include "linkas_files_qt/Tamac_functions.h"
#include "LinkasSerialPortLister/LinkasSerialPortLister.h"
#include "QDebug"
namespace Ui {
    class smartlinkTest;
}

class smartlinkTest : public QMainWindow
{
    Q_OBJECT

public:
    explicit smartlinkTest(QWidget *parent = 0);
    ~smartlinkTest();
    bool test;

private:
    Ui::smartlinkTest *ui;
    LinkasSerialPortThread *serialPort;
    QList<QString> port_list;
    void getExistingPorts();
    void processIncomingData(char* data);
    QString tamac_sentence;
    void fillBaudCombo();
    void processInfoMessage(char* data);
    void processGSMversion(char* data);
    QString convertToBinary(int number);



private slots:
    //General
    void slot_openPort();
    void slot_closePort();
    void slot_send();
    void slot_request_imei();
    void slot_request_GSMversion();
    void customMessageBox(QString);
    void fillPortsCombo();
    void checkSoftVer();
    void slot_setSerialNumber();
    void slot_setIP();
    void slot_setIP2();
    void slot_setPort();
    void slot_readSerialNumber();
    void slot_readIP();
    void slot_readPort();
    void slot_upDebug();
    void slot_downDebug();
    void slot_resetFields();
    void slot_request_software();
    void openParameterForm();
    void slot_request_APN();
    void slot_setAPN();
    //SerialPort
    void slot_newDataReceived(QChar dataReceived);



};

#endif // SMARTLINKTEST_H
