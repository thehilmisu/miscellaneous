/////Hakan
/////smartlink test software v1.0

#include "smartlinktest.h"
#include "ui_smartlinktest.h"

smartlinkTest::smartlinkTest(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::smartlinkTest)
{
    ui->setupUi(this);
    fillPortsCombo();fillBaudCombo();

    serialPort = new LinkasSerialPortThread();
    test = false;

    //connect
    connect(ui->btnRefresh,SIGNAL(clicked()),this,SLOT(fillPortsCombo()));
    connect(ui->btnConnect, SIGNAL(clicked()), this, SLOT(slot_openPort()));
    connect(ui->btnDisconnect, SIGNAL(clicked()), this, SLOT(slot_closePort()));
    connect(ui->btnIMEI,SIGNAL(clicked()),this,SLOT(slot_request_imei()));
    connect(ui->btnCheckSoftVer,SIGNAL(clicked()),this,SLOT(slot_request_GSMversion()));
    connect(ui->btnSetSerialNumber,SIGNAL(clicked()),this,SLOT(slot_setSerialNumber()));
    connect(ui->btnIP,SIGNAL(clicked()),this,SLOT(slot_setIP()));
    connect(ui->btnIP_2,SIGNAL(clicked()),this,SLOT(slot_setIP2()));
    connect(ui->btnPort,SIGNAL(clicked()),this,SLOT(slot_setPort()));
    connect(ui->btnSerialRead,SIGNAL(clicked()),this,SLOT(slot_readSerialNumber()));
    connect(ui->btnIPRead,SIGNAL(clicked()),this,SLOT(slot_readIP()));
    connect(ui->btnPortRead,SIGNAL(clicked()),this,SLOT(slot_readPort()));
    connect(ui->btnCloseSoftVer,SIGNAL(clicked()),this,SLOT(slot_downDebug()));
    connect(ui->btnOpenSoftVer,SIGNAL(clicked()),this,SLOT(slot_upDebug()));
    connect(ui->btnReset,SIGNAL(clicked()),this,SLOT(slot_resetFields()));
    connect(ui->actionParametreler,SIGNAL(triggered()),this,SLOT(openParameterForm()));
    connect(ui->btnYazVer,SIGNAL(clicked()),this,SLOT(slot_request_software()));
    connect(ui->btnWriteAPN,SIGNAL(clicked()),this,SLOT(slot_setAPN()));
    connect(ui->btnReadAPN,SIGNAL(clicked()),this,SLOT(slot_request_APN()));
    connect(serialPort, SIGNAL(dataReceived(QChar)), this, SLOT(slot_newDataReceived(QChar)));


    ui->lblConnected->setVisible(false);
    ui->lblDisconnected->setVisible(true);
    ui->lblIMEICheck->setVisible(false);
    ui->lblIMEICross->setVisible(true);
    ui->lblSoftCheck->setVisible(false);
    ui->lblSoftCross->setVisible(true);
    ui->lblSerialCheck->setVisible(false);
    ui->lblSerialCross->setVisible(true);
    ui->lblIPCheck->setVisible(false);
    ui->lblIPCross->setVisible(true);
    ui->lblPortCheck->setVisible(false);
    ui->lblPortCross->setVisible(true);
    ui->lblGPSCheck->setVisible(false);
    ui->lblGPSCross->setVisible(true);
    ui->lblEngineCheck->setVisible(false);
    ui->lblEngineCross->setVisible(true);
    ui->lblServerCheck->setVisible(false);
    ui->lblServerCross->setVisible(true);
    ui->lblIgnitionCheck->setVisible(false);
    ui->lblIgnitionCross->setVisible(true);
    ui->lblOpenDebugCheck->setVisible(false);
    ui->lblOpenDebugCross->setVisible(true);
    ui->lblCloseDebugCheck->setVisible(false);
    ui->lblCloseDebugCross->setVisible(true);
    ui->lblYazVerCheck->setVisible(false);
    ui->lblYazVerCross->setVisible(true);
    //ui->txtIncoming->insertHtml();

    ui->comboBaud->setCurrentIndex(2);

    ui->groupBox_2->setVisible(false);
    ui->groupBox_3->setVisible(false);
    ui->mainToolBar->setVisible(false);
    ui->menuBar->setVisible(false);

    this->setWindowTitle("Smartlink APN");
}

smartlinkTest::~smartlinkTest()
{
    serialPort->close();
    delete ui;
}
void smartlinkTest::slot_openPort()
{
    if(serialPort->isOpen())
    {
        serialPort->close();
    }

    serialPort->setPort(ui->comboExistingPorts->currentText()); //Port

    serialPort->setBaudRate(BAUD19200); //BaudRate

    serialPort->setDataBits(DATA_8); //DataBits

    serialPort->setParity(PAR_NONE); //Parity

    serialPort->setStopBits(STOP_1); //StopBits

    serialPort->setFlowControl(FLOW_DISABLE_DTR); //FlowControl


    serialPort->open();
    if(serialPort->isOpen()){
        ui->lblConnected->setVisible(true);
        ui->lblDisconnected->setVisible(false);
    }
}
void smartlinkTest::slot_closePort()
{
    serialPort->close();
    if(!serialPort->isOpen()){
        ui->lblConnected->setVisible(false);
        ui->lblDisconnected->setVisible(true);
    }
}
void smartlinkTest::slot_request_imei()
{

    char tamac_str[500];
    U_32 tamac_id = 74;
    ui->txtIMEINumber->clear();
    if(serialPort->isOpen())
    {
        Tamac_str_make_from_str(_GET_VAL, tamac_id, "", tamac_str);

        serialPort->send(QString(tamac_str));
        serialPort->send(QChar(_CR));
        serialPort->send(QChar(_LF));
        Convert_string_to_visible_chars(tamac_str);
        ui->txtIncoming_2->insertPlainText(QString(tamac_str)+"\n");

    }

}
void smartlinkTest::slot_request_APN()
{
    qDebug() << "request";
    char tamac_str[500];
    U_32 tamac_id = 42;
    ui->txtAPNread->clear();
    if(serialPort->isOpen())
    {
        Tamac_str_make_from_str(_GET_VAL, tamac_id, "", tamac_str);

        serialPort->send(QString(tamac_str));
        serialPort->send(QChar(_CR));
        serialPort->send(QChar(_LF));
        Convert_string_to_visible_chars(tamac_str);
        ui->txtIncoming_2->insertPlainText(QString(tamac_str)+"\n");

    }
}

void smartlinkTest::slot_send()
{
    serialPort->send("test");
}
void smartlinkTest::slot_newDataReceived(QChar dataReceived)
{

    qDebug() << "test";
    if( (dataReceived == QChar(_EOT)) || (dataReceived == QChar(_CR)) || (dataReceived == QChar(_LF)))
    {
        tamac_sentence.append(dataReceived);

        char *tamac_sentence_ch = tamac_sentence.toLocal8Bit().data();

        processIncomingData(tamac_sentence_ch);
        if(tamac_sentence.contains(">>q"))
        {
            processInfoMessage(tamac_sentence_ch);
        }
        if(tamac_sentence.contains("10."))
        {
            processGSMversion(tamac_sentence_ch);
        }
        Convert_string_to_visible_chars(tamac_sentence_ch);
        ui->txtIncoming->insertPlainText(QString(tamac_sentence_ch)+"\n");


    }
    else if(dataReceived == QChar(_SOH))
    {
        tamac_sentence.clear();
        tamac_sentence.append(dataReceived);
    }
    else
    {
        tamac_sentence.append(dataReceived);

    }

    //ui->txtIncoming_2->insertPlainText("####"+tamac_sentence+"####\n");

}
void smartlinkTest::customMessageBox(QString message)
{
    QMessageBox msgBox;
    msgBox.setText(message);
    msgBox.setStandardButtons(QMessageBox::Ok );
    msgBox.setDefaultButton(QMessageBox::Ok);
    msgBox.exec();
}
void smartlinkTest::getExistingPorts()
{
    LinkasSerialPortLister portLister;
    port_list.clear();
    QList<QextPortInfo> ports = portLister.getActivePortList();
    for(int i=0;i<ports.size();i++)
    {
        port_list.append(ports[i].portName);

    }

}
void smartlinkTest::fillPortsCombo()
{
    ui->comboExistingPorts->clear();
    getExistingPorts();
    for(int i = 0;i<port_list.size();i++)
    {
        ui->comboExistingPorts->insertItem(i,port_list.at(i));
    }
}
void smartlinkTest::processIncomingData(char* data)
{
    qDebug() << data;

    S_8 packet_id;
    U_32 param_no;
    char param_strval[300];


    packet_id = Tamac_get_packet_id(data);

    param_no = Tamac_get_param_no(data);
    //ui->txtIncoming_2->insertPlainText(QString::number(param_no));

    if(param_no == 74) //imei number
    {
        Tamac_get_group_alldata(data, param_strval);
        ui->txtIMEINumber->setText(QString(param_strval));
        ui->lblIMEICheck->setVisible(true);
        ui->lblIMEICross->setVisible(false);
    }
    else if(param_no == 193) // gsm soft. ver.
    {
        Tamac_get_group_alldata(data, param_strval);
        ui->txtSoftVersion->setText(QString(param_strval));


    }
    else if(param_no == 70) //set serial number
    {

          Tamac_get_group_alldata(data, param_strval);
          ui->txtSerialRead->setText(QString(param_strval));
          ui->lblSerialCheck->setVisible(true);
          ui->lblSerialCross->setVisible(false);

    }
    else if(param_no == 75) // set server1 ip
    {
        ui->lblIPCheck->setVisible(true);
        ui->lblIPCross->setVisible(false);
        Tamac_get_group_alldata(data, param_strval);
        ui->txtIPRead->setText(QString(param_strval));
    }
    else if(param_no == 10) // server1 port
    {
        Tamac_get_group_alldata(data, param_strval);
        U_32 converted_val = convert_tad64_to_U32(param_strval);
        ui->txtPortRead->setText(QString::number(converted_val));
        ui->lblPortCheck->setVisible(true);
        ui->lblPortCross->setVisible(false);
    }
    else if(param_no == 952)
    {
        Tamac_get_group_alldata(data, param_strval);
        U_32 converted = convert_tad64_to_U32(param_strval);

        qDebug()<<QString::number(converted);
        if(QString::number(converted) == "100")
        {
            ui->lblCloseDebugCheck->setVisible(true);
            ui->lblCloseDebugCross->setVisible(false);
        }
        else if(QString::number(converted) == "255")
        {
            ui->lblOpenDebugCheck->setVisible(true);
            ui->lblOpenDebugCross->setVisible(false);
        }
        //ui->lblStatus->setText(param_strval);

    }
    else if(param_no == 1)
    {
        Tamac_get_group_alldata(data, param_strval);
        U_32 converted = convert_tad64_to_U32(param_strval);
        ui->txtYazVer->setText(QString::number(converted));
        ui->lblYazVerCheck->setVisible(true);
        ui->lblYazVerCross->setVisible(false);
    }
    else if(param_no == 77)//apn
    {
        Tamac_get_group_alldata(data, param_strval);
        ui->txtAPNread->setText(QString(param_strval));

    }
    else if(param_no == 90)
    {
        Tamac_get_group_alldata(data, param_strval);
        ui->txtAPNread->setText(QString(param_strval));
    }


}
void smartlinkTest::slot_request_GSMversion()
{

    ui->lblSoftCheck->setVisible(false);
    ui->lblSoftCross->setVisible(true);
    char tamac_str[500];
    U_32 tamac_id = 193;
    char tamac_valuestr[300];
    if(serialPort->isOpen())
    {
        strcpy(tamac_valuestr, "AT+CGMR" );
        Tamac_str_make_from_str(_SET_STR, tamac_id, tamac_valuestr, tamac_str);
        serialPort->send(QString(tamac_str));

        Convert_string_to_visible_chars(tamac_str);
        ui->txtIncoming_2->insertPlainText(QString(tamac_str)+"\n");
    }
    ui->txtSoftVersion->clear();
}

void smartlinkTest::checkSoftVer()
{

}
void smartlinkTest::slot_setSerialNumber()
{
    char tamac_str[500];
    U_32 tamac_id = 70;
    char tamac_valuestr[300];

    if(serialPort->isOpen())
    {
        strcpy(tamac_valuestr, QString(ui->txtSerialNumber->text()).toLocal8Bit().data());
        Tamac_str_make_from_str(_SET_STR, tamac_id, tamac_valuestr, tamac_str);
        serialPort->send(QString(tamac_str));

        Convert_string_to_visible_chars(tamac_str);
        ui->txtIncoming_2->insertPlainText(QString(tamac_str)+"\n");
    }
    ui->txtSerialRead->clear();
}
void smartlinkTest::slot_setIP()
{
    char tamac_str[500];
    U_32 tamac_id = 75;
    char tamac_valuestr[300];

    if(serialPort->isOpen())
    {
        strcpy(tamac_valuestr, QString(ui->txtIP->text()).toLocal8Bit().data());
        Tamac_str_make_from_str(_SET_STR, tamac_id, tamac_valuestr, tamac_str);
        serialPort->send(QString(tamac_str));

        Convert_string_to_visible_chars(tamac_str);
        ui->txtIncoming_2->insertPlainText(QString(tamac_str)+"\n");
    }
    ui->txtIPRead->clear();
}
void smartlinkTest::slot_setAPN()
{
    char tamac_str[500];
    U_32 tamac_id = 77;
    char tamac_valuestr[300];

    if(serialPort->isOpen())
    {
        strcpy(tamac_valuestr, QString(ui->txtAPNWrite->text()).toLocal8Bit().data());
        Tamac_str_make_from_str(_SET_STR, tamac_id, tamac_valuestr, tamac_str);
        serialPort->send(QString(tamac_str));

        Convert_string_to_visible_chars(tamac_str);
        ui->txtIncoming_2->insertPlainText(QString(tamac_str)+"\n");
    }
    ui->txtAPNread->clear();
}

void smartlinkTest::slot_setIP2()
{
    char tamac_str[500];
    U_32 tamac_id = 75;
    char tamac_valuestr[300];

    if(serialPort->isOpen())
    {
        strcpy(tamac_valuestr, QString(ui->txtIP_2->text()).toLocal8Bit().data());
        Tamac_str_make_from_str(_SET_STR, tamac_id, tamac_valuestr, tamac_str);
        serialPort->send(QString(tamac_str));

        Convert_string_to_visible_chars(tamac_str);
        ui->txtIncoming_2->insertPlainText(QString(tamac_str)+"\n");
    }
}
void smartlinkTest::slot_setPort()
{
    char tamac_str[500];
    U_32 tamac_id;
    U_32 tamac_value;

    if(serialPort->isOpen())
    {
        tamac_id = 10;
        tamac_value = ui->txtPort->text().toInt();

        Tamac_str_make_from_int(_SET_INT, tamac_id, tamac_value, tamac_str);

        serialPort->send(QString(tamac_str));
        Convert_string_to_visible_chars(tamac_str);
        ui->txtIncoming_2->insertPlainText(QString(tamac_str));

    }
    ui->txtPortRead->clear();
}
void smartlinkTest::fillBaudCombo()
{

    ui->comboBaud->insertItem(0,"BAUD9600");
    ui->comboBaud->insertItem(1,"BAUD14400");
    ui->comboBaud->insertItem(2,"BAUD19200");
    ui->comboBaud->insertItem(3,"BAUD38400");
    ui->comboBaud->insertItem(4,"BAUD56000");
    ui->comboBaud->insertItem(5,"BAUD57600");
    ui->comboBaud->insertItem(6,"BAUD115200");
    ui->comboBaud->insertItem(7,"BAUD128000");
    ui->comboBaud->insertItem(8,"BAUD256000");
    ui->comboBaud->insertItem(9,"BAUD460800");

}
void smartlinkTest::processInfoMessage(char* data)
{
    QString str_data = QString(data);
    QStringList splitted_data = str_data.split(',');
    QString info = splitted_data[1];

    bool ok;
    U_8 int_info = (QString(info.at(1))+QString(info.at(2))).toInt(&ok,16);


    //ui->textEdit->insertPlainText(QString(buffer) +"#");


    QString rawSignalInfo = splitted_data[0];
    QString signalInfo = rawSignalInfo.mid(rawSignalInfo.indexOf("q")+1,rawSignalInfo.count());
    ui->lblSignal->setText(signalInfo);
    ui->lblSignal->setStyleSheet("QLabel {color : red; }");


    if((int_info & _BIT0) == 0)
    {
        //gps not valid
        ui->lblGPSCheck->setVisible(false);
        ui->lblGPSCross->setVisible(true);
    }
    else
    {
        //gps valid
        ui->lblGPSCheck->setVisible(true);
        ui->lblGPSCross->setVisible(false);
    }

    if((int_info & _BIT1) == 0)
    {
        //Engine not running
        ui->lblEngineCheck->setVisible(false);
        ui->lblEngineCross->setVisible(true);
    }
    else
    {
        //Engine running
        ui->lblEngineCheck->setVisible(true);
        ui->lblEngineCross->setVisible(false);
    }
    if((int_info & _BIT2) == 0)
    {
        //server not connected
        ui->lblServerCheck->setVisible(false);
        ui->lblServerCross->setVisible(true);
    }
    else
    {
        //server connected
        ui->lblServerCheck->setVisible(true);
        ui->lblServerCross->setVisible(false);
    }
    if((int_info & _BIT4) == 0)
    {
        //ignition off
        ui->lblIgnitionCheck->setVisible(false);
        ui->lblIgnitionCross->setVisible(true);
    }
    else
    {
        //ignition on
        ui->lblIgnitionCheck->setVisible(true);
        ui->lblIgnitionCross->setVisible(false);
    }
    if((int_info & _BIT5) == 0)
    {
        //battery
        ui->lblPowerType->setText("Pil");
        ui->lblPowerType->setStyleSheet("QLabel {  color : blue; }");

    }
    else
    {
        //power source
        ui->lblPowerType->setText("G�� Kaynagi");
        ui->lblPowerType->setStyleSheet("QLabel {  color : red; }");
    }

}
void smartlinkTest::processGSMversion(char *data)
{

    //QString version = splitted_data[1];
    Convert_string_to_visible_chars(data);

    QString str_data = QString(data);
    QString splitted = str_data.mid(str_data.indexOf("[")+1,str_data.indexOf("]")-5);
    //ui->txtIncoming_2->insertPlainText(splitted + "\n");
    ui->txtSoftVersion->setText(splitted);
    ui->lblSoftCheck->setVisible(true);
    ui->lblSoftCross->setVisible(false);
}

void smartlinkTest::slot_readSerialNumber()
{
    char tamac_str[500];
    U_32 tamac_id = 70;

    if(serialPort->isOpen())
    {
        Tamac_str_make_from_str(_GET_VAL, tamac_id, "", tamac_str);

        serialPort->send(QString(tamac_str));
        serialPort->send(QChar(_CR));
        serialPort->send(QChar(_LF));
        Convert_string_to_visible_chars(tamac_str);
        ui->txtIncoming_2->insertPlainText(QString(tamac_str)+"\n");

    }
    ui->txtSerialRead->clear();
}
void smartlinkTest::slot_readIP()
{
    char tamac_str[500];
    U_32 tamac_id = 75;

    if(serialPort->isOpen())
    {
        Tamac_str_make_from_str(_GET_VAL, tamac_id, "", tamac_str);

        serialPort->send(QString(tamac_str));
        serialPort->send(QChar(_CR));
        serialPort->send(QChar(_LF));
        Convert_string_to_visible_chars(tamac_str);
        ui->txtIncoming_2->insertPlainText(QString(tamac_str)+"\n");

    }
    ui->txtPortRead->clear();
}
void smartlinkTest::slot_readPort()
{
    char tamac_str[500];
    U_32 tamac_id = 10;

    if(serialPort->isOpen())
    {
        Tamac_str_make_from_str(_GET_VAL, tamac_id, "", tamac_str);

        serialPort->send(QString(tamac_str));
        serialPort->send(QChar(_CR));
        serialPort->send(QChar(_LF));
        Convert_string_to_visible_chars(tamac_str);
        ui->txtIncoming_2->insertPlainText(QString(tamac_str)+"\n");

    }
    ui->txtPortRead->clear();
}
void smartlinkTest::slot_upDebug()
{
    char tamac_str[500];
    U_32 tamac_id;
    U_32 tamac_value;

    if(serialPort->isOpen())
    {
        tamac_id = 952;
        tamac_value = 255;//ui->txtPort->text().toInt();

        Tamac_str_make_from_int(_SET_INT, tamac_id, tamac_value, tamac_str);

        serialPort->send(QString(tamac_str));
        Convert_string_to_visible_chars(tamac_str);
        ui->txtIncoming_2->insertPlainText(QString(tamac_str));

        Tamac_str_make_from_str(_GET_VAL, tamac_id, "", tamac_str);
        serialPort->send(QString(tamac_str));

    }
}
void smartlinkTest::slot_downDebug()
{
    char tamac_str[500];
    U_32 tamac_id;
    U_32 tamac_value;

    if(serialPort->isOpen())
    {
        tamac_id = 952;
        tamac_value = 100;//ui->txtPort->text().toInt();

        Tamac_str_make_from_int(_SET_INT, tamac_id, tamac_value, tamac_str);

        serialPort->send(QString(tamac_str));
        Convert_string_to_visible_chars(tamac_str);
        ui->txtIncoming_2->insertPlainText(QString(tamac_str));

        Tamac_str_make_from_str(_GET_VAL, tamac_id, "", tamac_str);
        serialPort->send(QString(tamac_str));

    }
}
void smartlinkTest::slot_resetFields()
{
    ui->lblConnected->setVisible(false);
    ui->lblDisconnected->setVisible(true);
    ui->lblIMEICheck->setVisible(false);
    ui->lblIMEICross->setVisible(true);
    ui->lblSoftCheck->setVisible(false);
    ui->lblSoftCross->setVisible(true);
    ui->lblSerialCheck->setVisible(false);
    ui->lblSerialCross->setVisible(true);
    ui->lblIPCheck->setVisible(false);
    ui->lblIPCross->setVisible(true);
    ui->lblPortCheck->setVisible(false);
    ui->lblPortCross->setVisible(true);
    ui->lblGPSCheck->setVisible(false);
    ui->lblGPSCross->setVisible(true);
    ui->lblEngineCheck->setVisible(false);
    ui->lblEngineCross->setVisible(true);
    ui->lblServerCheck->setVisible(false);
    ui->lblServerCross->setVisible(true);
    ui->lblIgnitionCheck->setVisible(false);
    ui->lblIgnitionCross->setVisible(true);
    ui->lblYazVerCheck->setVisible(false);
    ui->lblYazVerCross->setVisible(true);
    ui->lblCloseDebugCheck->setVisible(false);
    ui->lblCloseDebugCross->setVisible(true);
    ui->lblOpenDebugCheck->setVisible(false);
    ui->lblOpenDebugCross->setVisible(true);


    serialPort->close();

    ui->txtIMEINumber->clear();
    ui->txtIncoming_2->clear();
    ui->txtIncoming->clear();
    ui->txtIPRead->clear();
    ui->txtSerialNumber->clear();
    ui->txtSerialRead->clear();
    ui->txtSoftVersion->clear();
    ui->txtPortRead->clear();
    ui->txtYazVer->clear();

    ui->lblPowerType->setText("");
    ui->lblSignal->setText("");
}
void smartlinkTest::slot_request_software()
{
    char tamac_str[500];
    U_32 tamac_id = 1;

    if(serialPort->isOpen())
    {
        Tamac_str_make_from_str(_GET_VAL, tamac_id, "", tamac_str);

        serialPort->send(QString(tamac_str));
        Convert_string_to_visible_chars(tamac_str);
        ui->txtIncoming_2->insertPlainText(QString(tamac_str)+"\n");

    }
    ui->txtYazVer->clear();
}

void smartlinkTest::openParameterForm()
{


}
