//********************************************************************************************
//version : 1.0.0
//  *initial release
//  *thread'in deconstructor'� g�zden ge�irilecek
//version : 2.0.0
//  *thread'in %100 �al��mamas� i�in d�zenleme yap�ld�
//  *thread'in deconstructor'� d�zeltildi
//********************************************************************************************
#ifndef LinkasSerialPortThread_H
#define LinkasSerialPortThread_H

#include <QThread>
#include <QQueue>
#include <QMetaType>
#include <QMutex>
#include "qextserialport.h"

Q_DECLARE_METATYPE(BaudRateType)

Q_DECLARE_METATYPE(DataBitsType)

Q_DECLARE_METATYPE(ParityType)

Q_DECLARE_METATYPE(StopBitsType)

Q_DECLARE_METATYPE(FlowType)

class LinkasSerialPortThread : public QThread
{
    Q_OBJECT

public:
    LinkasSerialPortThread();
    LinkasSerialPortThread( QString portName, BaudRateType baudRate,
                            DataBitsType dataBits,ParityType parity,
                            StopBitsType stopBits,  FlowType flowControl
                          );
    ~LinkasSerialPortThread();
    bool open();
    bool isOpen();
    void close();
    bool send(QChar data);
    bool send(QString data);

    bool setPort(QString portName);
    QString getPort();
    void setBaudRate(BaudRateType baudRate);
    QString getBaudRate();
    void setDataBits(DataBitsType dataBits);
    QChar getDataBits();
    void setParity(ParityType parity);
    QString getParity();
    void setStopBits(StopBitsType stopBits);
    QString getStopBit();
    void setFlowControl(FlowType flowControl);
    QString getFlowControl();

signals:
    void dataReceived(QChar data);

private:
    QextSerialPort* extSerialPort;
    QMutex* extSerialPortMutex;
    QQueue<QString>sendStringQueue;
    QMutex* sendStringQueueMutex;

protected:
    void run();
};
#endif // LinkasSerialPortThread_H
