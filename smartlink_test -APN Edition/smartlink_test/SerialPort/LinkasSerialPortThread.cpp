#include "LinkasSerialPortThread.h"

LinkasSerialPortThread::LinkasSerialPortThread()
{
    extSerialPortMutex = new QMutex();
    sendStringQueueMutex = new QMutex();

    extSerialPortMutex->lock();
    extSerialPort = new QextSerialPort();
    extSerialPort->setPortName("");
    extSerialPort->setBaudRate(BAUD9600);
    extSerialPort->setDataBits(DATA_8);
    extSerialPort->setParity(PAR_NONE);
    extSerialPort->setStopBits(STOP_1);
    extSerialPort->setFlowControl(FLOW_OFF);
    extSerialPortMutex->unlock();

    sendStringQueueMutex->lock();
    sendStringQueue.clear();
    sendStringQueueMutex->unlock();

    this->start();
}
LinkasSerialPortThread::LinkasSerialPortThread( QString portName, BaudRateType baudRate,
                                                DataBitsType dataBits,ParityType parity,
                                                StopBitsType stopBits,  FlowType flowControl
                                              )
{
    extSerialPortMutex = new QMutex();
    sendStringQueueMutex = new QMutex();

    extSerialPortMutex->lock();
    extSerialPort = new QextSerialPort();
    extSerialPort->setPortName(portName);
    extSerialPort->setBaudRate(baudRate);
    extSerialPort->setDataBits(dataBits);
    extSerialPort->setParity(parity);
    extSerialPort->setStopBits(stopBits);
    extSerialPort->setFlowControl(flowControl);
    extSerialPortMutex->unlock();

    sendStringQueueMutex->lock();
    sendStringQueue.clear();
    sendStringQueueMutex->unlock();

    this->start();
}

LinkasSerialPortThread::~LinkasSerialPortThread()
{
    extSerialPortMutex->lock();
    if(extSerialPort->isOpen())
    {
        extSerialPort->close();
    }
    delete extSerialPort;
    extSerialPortMutex->unlock();

    sendStringQueueMutex->lock();
    sendStringQueue.clear();
    sendStringQueueMutex->unlock();

    delete extSerialPortMutex;
    delete sendStringQueueMutex;

    this->terminate();
}

bool LinkasSerialPortThread::open()
{
    bool _isOpen = false;
    extSerialPortMutex->lock();
    extSerialPort->open(QIODevice::ReadWrite);
    if(extSerialPort->isOpen())
    {
        _isOpen = true;
    }
    extSerialPortMutex->unlock();
    return _isOpen;
}

bool LinkasSerialPortThread::isOpen()
{
    bool _isOpen = false;
    extSerialPortMutex->lock();
    _isOpen = extSerialPort->isOpen();
    extSerialPortMutex->unlock();
    return _isOpen;
}

void LinkasSerialPortThread::close()
{
    extSerialPortMutex->lock();
    extSerialPort->close();
    extSerialPortMutex->unlock();
}

bool LinkasSerialPortThread::send(QChar data)
{
    bool _isOK = false;
    extSerialPortMutex->lock();
    if(extSerialPort->isOpen())
    {
        sendStringQueueMutex->lock();
        sendStringQueue.enqueue(QString(data));
        sendStringQueueMutex->unlock();
        _isOK = true;
    }
    extSerialPortMutex->unlock();
    return _isOK;
}

bool LinkasSerialPortThread::send(QString data)
{
    bool _isOK = false;
    extSerialPortMutex->lock();
    if(extSerialPort->isOpen())
    {
        sendStringQueueMutex->lock();
        sendStringQueue.enqueue(data);
        sendStringQueueMutex->unlock();
        _isOK = true;
    }
    extSerialPortMutex->unlock();
    return _isOK;
}

bool LinkasSerialPortThread::setPort(QString portName)
{
    bool _isOK = false;
    extSerialPortMutex->lock();
    if(!extSerialPort->isOpen())
    {
        extSerialPort->setPortName(portName);
        _isOK = true;
    }
    extSerialPortMutex->unlock();
    return _isOK;
}

QString LinkasSerialPortThread::getPort()
{
    QString portName;
    extSerialPortMutex->lock();
    portName = extSerialPort->portName();
    extSerialPortMutex->unlock();
    return portName;
}

void LinkasSerialPortThread::setBaudRate(BaudRateType baudRate)
{
    extSerialPortMutex->lock();
    extSerialPort->setBaudRate(baudRate);
    extSerialPortMutex->unlock();
}

QString LinkasSerialPortThread::getBaudRate()
{
    QString baudRate;
    extSerialPortMutex->lock();
    switch(extSerialPort->baudRate())
    {
        case BAUD50:
        {
            baudRate = QString("50");
            break;
        }
        case BAUD75:
        {
            baudRate = QString("75");
            break;
        }
        case BAUD110:
        {
            baudRate = QString("110");
            break;
        }
        case BAUD134:
        {
            baudRate = QString("134");
            break;
        }
        case BAUD150:
        {
            baudRate = QString("150");
            break;
        }
        case BAUD200:
        {
            baudRate = QString("200");
            break;
        }
        case BAUD300:
        {
            baudRate = QString("300");
            break;
        }
        case BAUD600:
        {
            baudRate = QString("600");
            break;
        }
        case BAUD1200:
        {
            baudRate = QString("1200");
            break;
        }
        case BAUD1800:
        {
            baudRate = QString("1800");
            break;
        }
        case BAUD2400:
        {
            baudRate = QString("2400");
            break;
        }
        case BAUD4800:
        {
            baudRate = QString("4800");
            break;
        }
        case BAUD9600:
        {
            baudRate = QString("9600");
            break;
        }
        case BAUD14400:
        {
            baudRate = QString("14400");
            break;
        }
        case BAUD19200:
        {
            baudRate = QString("19200");
            break;
        }
        case BAUD38400:
        {
            baudRate = QString("38400");
            break;
        }
        case BAUD56000:
        {
            baudRate = QString("56000");
            break;
        }
        case BAUD57600:
        {
            baudRate = QString("57600");
            break;
        }
        case BAUD76800:
        {
            baudRate = QString("76800");
            break;
        }
        case BAUD115200:
        {
            baudRate = QString("115200");
            break;
        }
        case BAUD128000:
        {
            baudRate = QString("128000");
            break;
        }
        case BAUD256000:
        {
            baudRate = QString("256000");
            break;
        }
        case BAUD460800:
        {
            baudRate = QString("460800");
            break;
        }
        default:
        {
            baudRate = QString("");
            break;
        }
    }
    extSerialPortMutex->unlock();
    return baudRate;
}

void LinkasSerialPortThread::setDataBits(DataBitsType dataBits)
{
    extSerialPortMutex->lock();
    extSerialPort->setDataBits(dataBits);
    extSerialPortMutex->unlock();
}

QChar LinkasSerialPortThread::getDataBits()
{
    QChar dataBits;
    extSerialPortMutex->lock();
    switch(extSerialPort->dataBits())
    {
        case DATA_5:
        {
            dataBits = QChar('5');
            break;
        }
        case DATA_6:
        {
            dataBits = QChar('6');
            break;
        }
        case DATA_7:
        {
            dataBits = QChar('7');
            break;
        }
        case DATA_8:
        {
            dataBits = QChar('8');
            break;
        }
        default:
        {
            dataBits = QChar();
            break;
        }
    }
    extSerialPortMutex->unlock();
    return dataBits;
}

void LinkasSerialPortThread::setParity(ParityType parity)
{
    extSerialPortMutex->lock();
    extSerialPort->setParity(parity);
    extSerialPortMutex->unlock();
}

QString LinkasSerialPortThread::getParity()
{
    QString parity;
    extSerialPortMutex->lock();
    switch(extSerialPort->parity())
    {
        case PAR_NONE:
        {
            parity = QString("None");
            break;
        }
        case PAR_ODD:
        {
            parity = QString("Odd");
            break;
        }
        case PAR_EVEN:
        {
            parity = QString("Even");
            break;
        }
        case PAR_MARK:
        {
            parity = QString("Mark");
            break;
        }
        case PAR_SPACE:
        {
            parity = QString("Space");
            break;
        }
        default:
        {
            parity = QString("");
            break;
        }
    }
    extSerialPortMutex->unlock();
    return parity;
}

void LinkasSerialPortThread::setStopBits(StopBitsType stopBits)
{
    extSerialPortMutex->lock();
    extSerialPort->setStopBits(stopBits);
    extSerialPortMutex->unlock();
}

QString LinkasSerialPortThread::getStopBit()
{
    QString stopBits;
    extSerialPortMutex->lock();
    switch(extSerialPort->stopBits())
    {
        case STOP_1:
        {
            stopBits = QString("1");
            break;
        }
        case STOP_1_5:
        {
            stopBits = QString("1.5");
            break;
        }
        case STOP_2:
        {
            stopBits = QString("2");
            break;
        }
        default:
        {
            stopBits = QString("");
            break;
        }
    }
    extSerialPortMutex->unlock();
    return stopBits;
}

void LinkasSerialPortThread::setFlowControl(FlowType flowControl)
{
    extSerialPortMutex->lock();
    extSerialPort->setFlowControl(flowControl);
    extSerialPortMutex->unlock();
}

QString LinkasSerialPortThread::getFlowControl()
{
    QString flowControl;
    extSerialPortMutex->lock();
    switch(extSerialPort->flowControl())
    {
        case FLOW_OFF:
        {
            flowControl = QString("Off");
            break;
        }
        case FLOW_HARDWARE:
        {
            flowControl = QString("Hardware");
            break;
        }
        case FLOW_XONXOFF:
        {
            flowControl = QString("Xon/Xoff");
            break;
        }
        default:
        {
            flowControl = QString("");
            break;
        }
    }
    extSerialPortMutex->unlock();
    return flowControl;
}

void LinkasSerialPortThread::run()
{
    forever
    {
        bool _isReceiveIdle = true;
        bool _isSendIdle = true;
        extSerialPortMutex->lock();
        if(extSerialPort->isOpen())
        {
            {//receiving data
                int numBytes = 0;
                char data[2];
                numBytes = extSerialPort->bytesAvailable();
                if(numBytes > 0)
                {
                    extSerialPort->read(data,1);
                    data[1] = NULL;
                    QChar incoming_data = data[0];
                    emit dataReceived(incoming_data);
                    _isReceiveIdle = false;
                }
                else
                {
                    _isReceiveIdle = true;
                }
            }
            {//sending buffered data
                sendStringQueueMutex->lock();
                if(sendStringQueue.size() > 0)
                {
                    //Only ascii chars
                    QByteArray byteArray = sendStringQueue.dequeue().toAscii();
                    extSerialPort->write(byteArray, byteArray.size());
                    _isSendIdle = false;
                }
                else
                {
                    _isSendIdle = true;
                }
                sendStringQueueMutex->unlock();
            }
        }
        extSerialPortMutex->unlock();

        if(_isReceiveIdle && _isSendIdle)
        {
            this->msleep(100);
        }
    }
}
