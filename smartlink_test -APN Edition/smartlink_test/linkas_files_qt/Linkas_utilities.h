#ifndef __LINKASUTIL_H
#define __LINKASUTIL_H
//------------------------------------------------------------------------------
// LINKAS ELEKTRONIK LTD STI
// GERSAN SAN.SIT. 2310/1 SK NO:70
// 06370 BATIKENT ANKARA-TURKIYE
// www.linkas.com.tr
//
// (c) Copyright LINKAS ELEKTRONIK LTD STI
//
// Programmers  : TOC
// Description  : LINKAS utility functions
//
// VERSION : 1.9
//
// Version History:
//  V1.0: Initial relase
//  V1.1: 2009-09-01, TOC
//      1. strlen_for_interrupt() function is added 
//      2. strcpy_for_interrupt() function is added 
//  V1.2: 2009-09-04, TOC
//      3. 32-bit hatoi function hatoi32 is added,
//      4. 16-bit hatio function is renamed as hatoi16
//  V1.3: 2010-02-28, TOC
//      5. atoi functions are added
//  V1.4: 2010-03-08, TOC
//      6. tad64 conversion and CS functions are added
//  V1.5: 2010-07-29, TOC
//      7. hatoi64 is added
//  V1.6: 2011-05-14
//      8. Getting number and string from a string functions are added
//      10. Getting string from a string functions are added
//      11. Value_limit function is added
//  V1.7: 2011-08-09
//      12. Getting string from a string function is corrected
//  V1.8: 2011-10-09
//      13. Get_limitedstring_from_string is added.
//      14. Return string len of Getstring_from_string function is increased from 30 t0 300
//  V1.9: 2011-11-12
//      15. String_to_substring fonksiyonu eklendi.
//      16. String_contains_substring fonksiyonu eklendi
//  V2.0: 2011-12-11
//      17. strcpy_for_interrupt() function is renamed as Strlcpy_for_interrupt(), and copy limit is added.
//
//------------------------------------------------------------------------------

#include <string.h>
#include "Linkas_constants.h"
#include "Linkas_datatypes_Qt.h"



//------------------------------------------------------------------------------
//  hatoi16 
//  Unsigned HEX string to 16bit unsigned integer value converter
//  input  -> ASCII HEX string (max 4 bytes) 
//  output -> unsigned integer value
//------------------------------------------------------------------------------
extern U_16 hatoi16 (char *hexmetin);


//------------------------------------------------------------------------------
//  hatoi32
//  Unsigned HEX string to 32bit unsigned integer value converter
//  input  -> ASCII HEX string (max 8 bytes)
//  output -> unsigned integer value
//------------------------------------------------------------------------------
extern U_32 hatoi32 (char *hexmetin);

//------------------------------------------------------------------------------
//  hatoi64
//  Unsigned HEX string to 64bit unsigned integer value converter
//  input  -> ASCII HEX string (max 16 bytes)
//  output -> unsigned integer value
//------------------------------------------------------------------------------
extern U_64 hatoi64 (char *hexmetin);


//------------------------------------------------------------------------------
//  Get string lenght for interrupt routines
//  input  -> input string to be tested 
//  output -> lenght of input string
//------------------------------------------------------------------------------
extern U_16 strlen_for_interrupt(char *instring);


//------------------------------------------------------------------------------
//  Copy source string to destination string for interrupt routines
//  input  -> source and destination strings
//  output -> none
//------------------------------------------------------------------------------
extern void Strlcpy_for_interrupt(char *destination, char *source, U_16 copy_len);


//------------------------------------------------------------------------------
//  Convert ascii numbers to unsigned interger numbers
//  input  -> ascii number
//  output -> U32 or U64 interger number
//------------------------------------------------------------------------------
extern U_32 Linkas_atoi_U32(char *metin);
extern U_64 Linkas_atoi_U64(char *metin);


//------------------------------------------------------------------------------
//  Conver U_32 number to tad64-formatted string
//  input  -> U_32 number to be converted
//  output -> tad64 string
//------------------------------------------------------------------------------
extern void Convert_U32_to_tad64(U_32 inputnumber, char *ret_tad64);

//------------------------------------------------------------------------------
//  Conver U_32 number to tad64-formatted fixed lenght string, fill left parts with 0
//  input  -> U_32 number to be converted
//  output -> tad64 string, zero padded
//------------------------------------------------------------------------------
extern void Convert_U32_to_tad64_fixlenght(U_32 inputnumber, char *ret_tad64, U_8 tad64_len);


//------------------------------------------------------------------------------
//  Conver tad64-formatted string to integer number 
//  input  -> tad64 string to be converted
//  output -> U_32 or U_64 number
//------------------------------------------------------------------------------
extern U_64 convert_tad64_to_U64(char *str_tad64);
extern U_32 convert_tad64_to_U32(char *str_tad64);


//------------------------------------------------------------------------------
//  Find TAD64 string checksum of input string
//  input  -> string for finding 2-digit CS
//  output -> 2 digit checksum string with null termination
//------------------------------------------------------------------------------
extern void String_CS_tad64(char *input_string, char *cs_tad64_string_out);


//------------------------------------------------------------------------------
//  find tad64 checksum of the input string, return it as U_16 (only 12-bits are non-zero)
//  input  -> string for finding 12-bits CS
//  output -> return U_16 tad64 checksum
//------------------------------------------------------------------------------
extern U_16 String_u16CS_tad64(char *in_str);


//------------------------------------------------------------------------------
//  check TAD64 string checksum of input string, which appears at last 2 chars
//  input  -> string for checking 2-digit CS
//  output -> TRUE if csum matches, FALSE otherwise
//------------------------------------------------------------------------------
extern TBOOL Check_str_CS_tad64(char *in_str);      // check last 2 chars of input string for TAD64 checksum


//------------------------------------------------------------------------------
//  for debugging and tracing, convert string with invisible chars to all visible chars
//  input  -> string with invisible chars
//  output -> string with invisible chars converted to visible chars as below
//      _DC1   -->    +
//      _DC2   -->    -
//      _DC3   -->    *
//      _DC4   -->    /
//      _ACK   -->    <
//      _NAK   -->    >
//      _SOH   -->    {
//      _EOT   -->    }
//      _STX   -->    [
//      _ETX   -->    ]
//      _GS    -->    _
//------------------------------------------------------------------------------
extern void Convert_string_to_visible_chars(char *instr);


//------------------------------------------------------------------------------
//  left shift input string with given number
//  input  -> string to be leftshifted, shift-number
//  output -> shifted string
//------------------------------------------------------------------------------
extern void String_leftshift(char *instr, U_16 shiftno);

//------------------------------------------------------------------------------
//  Find substring from a given string. Substring starts from start_index, and ends at end_index;
//  input  -> original string, start_index (starts from 0), end_index (starts from 0)
//  output -> substring
//------------------------------------------------------------------------------
extern void String_to_substring(char *instr, char *outstr, S_16 start_index, S_16 end_index);

//------------------------------------------------------------------------------
//  Returns the position of substring in source string. If substring is not found, returns -1
//  search starts from start_index
//  input  -> original string,
//  output -> substring position (starts from 0)
//------------------------------------------------------------------------------
extern S_16 String_contains_substring(char *instr, char *substr, U_16 start_index);



//------------------------------------------------------------------------------
//  Get number or string from a given string
//  input  -> source string, sequence-or-position
//            sequence and position starts by 0
//  output -> found interger or string
//------------------------------------------------------------------------------
extern U_64  Getnumber_from_string(char *metin, U_16 sequence);                         // metin icinde ',' ';' '*' ':' karakterleri ile ayrilmis bolumlarden, belirtilen siradaki siyiyi verir.  Sira 0'dan baslar
extern U_64  Getnumber_from_string_atgiven_position(char *metin, U_16 position);        // metin icinde belirtilen konumdan baslayan ve ',' ';' '*' karakterleri ile biten araliktaki siyiyi verir. 
extern TBOOL Getstring_from_string(char *inputmetin, U_16 sequence, char *outputmetin); // metin icinde ',' ';' '*' ':' karakterleri ile ayrilmis bolumlarden, belirtilen siradaki metini verir.  Sira 0'dan baslar. Belirtilen sira varsa TRUE, yoksa FALSE dondurulur.
extern TBOOL Get_limitedstring_from_string(char *inputmetin, U_16 sequence, char *outputmetin, U_16 outmetin_len);  // Same as Getstring_from_string, except output string is limited to outmetin_len


//------------------------------------------------------------------------------
//  Return input number if (input < limit),  else return limit value
//  input  -> input number to be limited and limit value
//  output -> shifted string
//------------------------------------------------------------------------------
extern U_64 Value_limit(U_64 input_num, U_64 limit_val);




//----------------------------------------------------------------------------------
//	CONSTANT & DEFINITIONS
//----------------------------------------------------------------------------------
extern const char tad64mod[];
extern const char tad64_demod[];




#endif 
