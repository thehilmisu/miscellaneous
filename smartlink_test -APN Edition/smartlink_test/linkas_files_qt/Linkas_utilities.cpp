//--------------------------------------------------------------------------------------------------
// LINKAS ELEKTRONIK LTD STI
// GERSAN SAN.SIT 2310/1 SK NO:70
// 06370 BATIKENT ANKARA-TURKIYE
//
// (c) Copyright, LINKAS ELEKTRONIK LTD STI
//
//          
//--------------------------------------------------------------------------------------------------

#include "Linkas_utilities.h"

//--------------------------------------------------------------------------------------------------
//	FUNCTION PROTOTYPES
//--------------------------------------------------------------------------------------------------
void Convert_U32_to_tad64(U_32 inputnumber, char *ret_tad64);
void Convert_U32_to_tad64_fixlenght(U_32 inputnumber, char *ret_tad64, U_8 tad64_len);
U_64 Value_limit(U_64 input_num, U_64 limit_val);



//------------------------------------------------------------------------------------------------------
//	CONSTANT & DEFINITIONS
//------------------------------------------------------------------------------------------------------

const char tad64mod[64]=			// ascii modulasyon i�in kullan�lan tablo
{
			'0'	,	//	0
			'1'	,	//	1
			'2'	,	//	2
			'3'	,	//	3
			'4'	,	//	4
			'5'	,	//	5
			'6'	,	//	6
			'7'	,	//	7
			'8'	,	//	8
			'9'	,	//	9
			'A'	,	//	10
			'B'	,	//	11
			'C'	,	//	12
			'D'	,	//	13
			'E'	,	//	14
			'F'	,	//	15
			'G'	,	//	16
			'H'	,	//	17
			'I'	,	//	18
			'J'	,	//	19
			'K'	,	//	20
			'L'	,	//	21
			'M'	,	//	22
			'N'	,	//	23
			'O'	,	//	24
			'P'	,	//	25
			'Q'	,	//	26
			'R'	,	//	27
			'S'	,	//	28
			'T'	,	//	29
			'U'	,	//	30
			'V'	,	//	31
			'W'	,	//	32
			'X'	,	//	33
			'Y'	,	//	34
			'Z'	,	//	35
			'a'	,	//	36
			'b'	,	//	37
			'c'	,	//	38
			'd'	,	//	39
			'e'	,	//	40
			'f'	,	//	41
			'g'	,	//	42
			'h'	,	//	43
			'i'	,	//	44
			'j'	,	//	45
			'k'	,	//	46
			'l'	,	//	47
			'm'	,	//	48
			'n'	,	//	49
			'o'	,	//	50
			'p'	,	//	51
			'q'	,	//	52
			'r'	,	//	53
			's'	,	//	54
			't'	,	//	55
			'u'	,	//	56
			'v'	,	//	57
			'w'	,	//	58
			'x'	,	//	59
			'y'	,	//	60
			'z'	,	//	61
			'#'	,	//	62
			'$'		//	63
};
const char tad64_demod[128]=			// ascii de-modulasyon i�in kullan�lan tablo		
{					
		0	,	//	0
		0	,	//	1
		0	,	//	2
		0	,	//	3
		0	,	//	4
		0	,	//	5
		0	,	//	6
		0	,	//	7
		0	,	//	8
		0	,	//	9
		0	,	//	10
		0	,	//	11
		0	,	//	12
		0	,	//	13
		0	,	//	14
		0	,	//	15
		0	,	//	16
		0	,	//	17
		0	,	//	18
		0	,	//	19
		0	,	//	20
		0	,	//	21
		0	,	//	22
		0	,	//	23
		0	,	//	24
		0	,	//	25
		0	,	//	26
		0	,	//	27
		0	,	//	28
		0	,	//	29
		0	,	//	30
		0	,	//	31
		0	,	//	32
		0	,	//	33
		0	,	//	34
		62	,	//	35
		63	,	//	36
		0	,	//	37
		0	,	//	38
		0	,	//	39
		0	,	//	40
		0	,	//	41
		0	,	//	42
		63	,	//	43
		0	,	//	44
		62	,	//	45
		0	,	//	46
		0	,	//	47
		0	,	//	48
		1	,	//	49
		2	,	//	50
		3	,	//	51
		4	,	//	52
		5	,	//	53
		6	,	//	54
		7	,	//	55
		8	,	//	56
		9	,	//	57
		0	,	//	58
		0	,	//	59
		0	,	//	60
		0	,	//	61
		0	,	//	62
		0	,	//	63
		0	,	//	64
		10	,	//	65
		11	,	//	66
		12	,	//	67
		13	,	//	68
		14	,	//	69
		15	,	//	70
		16	,	//	71
		17	,	//	72
		18	,	//	73
		19	,	//	74
		20	,	//	75
		21	,	//	76
		22	,	//	77
		23	,	//	78
		24	,	//	79
		25	,	//	80
		26	,	//	81
		27	,	//	82
		28	,	//	83
		29	,	//	84
		30	,	//	85
		31	,	//	86
		32	,	//	87
		33	,	//	88
		34	,	//	89
		35	,	//	90
		0	,	//	91
		0	,	//	92
		0	,	//	93
		0	,	//	94
		0	,	//	95
		0	,	//	96
		36	,	//	97
		37	,	//	98
		38	,	//	99
		39	,	//	100
		40	,	//	101
		41	,	//	102
		42	,	//	103
		43	,	//	104
		44	,	//	105
		45	,	//	106
		46	,	//	107
		47	,	//	108
		48	,	//	109
		49	,	//	110
		50	,	//	111
		51	,	//	112
		52	,	//	113
		53	,	//	114
		54	,	//	115
		55	,	//	116
		56	,	//	117
		57	,	//	118
		58	,	//	119
		59	,	//	120
		60	,	//	121
		61	,	//	122
		0	,	//	123
		0	,	//	124
		0	,	//	125
		0	,	//	126
		0		//	127
};	

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
// FUNCTIONS 
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
U_16 hatoi16 (char *hexmetin)
{
	U_8  i=0;
	U_16 value=0;
	U_8  digit=0;
	U_8 metinlen=0;

	metinlen = strlen(hexmetin);

	for (i=0; i < metinlen; i++)
	{
		if((hexmetin[i] >= '0' )&&( hexmetin[i] <='9'))
		{
			value = value << 4;
			digit = hexmetin[i] - '0';
			value += digit;
		}
		else if((hexmetin[i] >= 'A' )&&( hexmetin[i] <= 'F' ))
		{
			value = value << 4;
			digit = hexmetin[i] - 'A' + 10;
			value += digit;
		}
   		else if((hexmetin[i] >= 'a' )&&( hexmetin[i] <= 'f' ))
		{
			value = value << 4;
			digit = hexmetin[i] - 'a' + 10;
			value += digit;
		}
		else
		{
			
		}
	}
  	return value;
}
//--------------------------------------------------------------------------------------------------
U_32 hatoi32 (char *hexmetin)	
{
    U_8  i=0;
    U_32 value=0; 
    U_8  digit=0;
	U_8 metinlen=0;

	metinlen = strlen(hexmetin);

	for (i=0; i < metinlen; i++)
	{
		if((hexmetin[i] >= '0' )&&( hexmetin[i] <='9'))
		{
			value = value << 4;
			digit = hexmetin[i] - '0';
			value += digit;
		}
		else if((hexmetin[i] >= 'A' )&&( hexmetin[i] <= 'F' ))
		{
			value = value << 4;
			digit = hexmetin[i] - 'A' + 10;
			value += digit;
		}
   		else if((hexmetin[i] >= 'a' )&&( hexmetin[i] <= 'f' ))
		{
			value = value << 4;
			digit = hexmetin[i] - 'a' + 10;
			value += digit;
		}
		else
		{
			
		}
	}
  	return value;
}
//--------------------------------------------------------------------------------------------------
U_64 hatoi64 (char *hexmetin)
{
    U_8  i=0;
    U_64 value=0;
    U_8  digit=0;
	U_8 metinlen=0;

	metinlen = strlen(hexmetin);

	for (i=0; i < metinlen; i++)
	{
		if((hexmetin[i] >= '0' )&&( hexmetin[i] <='9'))
		{
			value = value << 4;
			digit = hexmetin[i] - '0';
			value += digit;
		}
		else if((hexmetin[i] >= 'A' )&&( hexmetin[i] <= 'F' ))
		{
			value = value << 4;
			digit = hexmetin[i] - 'A' + 10;
			value += digit;
		}
   		else if((hexmetin[i] >= 'a' )&&( hexmetin[i] <= 'f' ))
		{
			value = value << 4;
			digit = hexmetin[i] - 'a' + 10;
			value += digit;
		}
		else
		{

		}
	}
  	return value;
}

//--------------------------------------------------------------------------------------------------
U_16 strlen_for_interrupt (char *instring)
{
	U_16 i;

	for(i=0;i<2000;i++)
	{
                if(instring[i] == NULL)	return i;
	}
	return i;
}

//--------------------------------------------------------------------------------------------------
void Strlcpy_for_interrupt (char *destination, char *source, U_16 copy_len)
{
	U_16 i;

	for(i=0;i<copy_len;i++)
	{
		destination[i] = source[i];
		if(source[i] == NULL) break;
	}
	destination[copy_len-1] = NULL;
}

//--------------------------------------------------------------------------------------------------
U_32 Linkas_atoi_U32 (char *metin)	        // ascii to U_32 conversion
{
	U_32 i, value, digit;

	value = 0;

	for(i=0;i<strlen(metin);i++)
	{
		if((metin[i]>='0')&&(metin[i]<='9'))
		{
			value = value * 10;
			digit = metin[i] - '0';
			value += digit;
		}
	}

  	return value;
}
//--------------------------------------------------------------------------------------------------
U_64 Linkas_atoi_U64 (char *metin)		    // ascii to U_64 conversion
{
	U_32 i, digit;
	U_64 value;

	value = 0;

	for(i=0;i<strlen(metin);i++)
	{
		if((metin[i]>='0')&&(metin[i]<='9'))
		{
			value = value * 10;
			digit = metin[i] - '0';
			value += digit;
		}
	}
  	return value;
}
//--------------------------------------------------------------------------------------------------
void Convert_U32_to_tad64 (U_32 inputnumber, char *ret_tad64)	// U_32 sayiyi tad64 formatina cevir
{
	U_8 i,shiftcount;

	shiftcount = 0;

	while(inputnumber != 0)
	{
		// stringi 1 hane saga kaydiralim
			for(i=shiftcount;i>0;i--)
			{
				ret_tad64[i] = ret_tad64[i-1];	
			}
		// en sola yeni tad64 karakterini koyalim
	 		ret_tad64[0] = tad64mod[inputnumber & 0x3F];
		// sayiyi kaydiralim ve shiftcount arttiralim
			inputnumber = inputnumber >> 6;
			shiftcount++;
	}

	// string sonlandiralim
		ret_tad64[shiftcount] = NULL;

	// sayi zaten sifirsa ayrica 0 yukleyelim
		if(shiftcount == 0)
		{
			ret_tad64[0] = tad64mod[0];
			ret_tad64[1] = NULL;
		}

}
//--------------------------------------------------------------------------------------------------
void Convert_U32_to_tad64_fixlenght (U_32 inputnumber, char *ret_tad64, U_8 tad64_len)	// U_32 sayiyi tad64 formatina, sabit uzunlukla cevirir, basina 0 doldurur.
{
	U_8 i;


   // stringi bulalim
		for(i=0;i<tad64_len;i++)
		{
			ret_tad64[tad64_len - i - 1] = tad64mod[inputnumber & 0x3F];
			inputnumber = inputnumber >> 6;	
		}

	// string sonlandiralim
		ret_tad64[tad64_len] = NULL;

	// tad64len sifirsa ayrica 0 yukleyelim
		if(tad64_len == 0)
		{
			ret_tad64[0] = tad64mod[0];
			ret_tad64[1] = NULL;
		}
}
//--------------------------------------------------------------------------------------------------
U_64 convert_tad64_to_U64 (char *str_tad64)
{
	U_64 sonuc;
	U_16 tad64_len,i;
	
	
	tad64_len = strlen(str_tad64);
	sonuc = 0;
	
	for(i=0;i<tad64_len;i++)
	{
		sonuc = (sonuc << 6) + tad64_demod[str_tad64[i]];	
	}

	return sonuc;
}
//--------------------------------------------------------------------------------------------------
U_32 convert_tad64_to_U32 (char *str_tad64)
{
	U_32 sonuc;
	U_16 tad64_len,i;
	
	
	tad64_len = strlen(str_tad64);
	sonuc = 0;
	
	for(i=0;i<tad64_len;i++)
	{
		sonuc = (sonuc << 6) + tad64_demod[str_tad64[i]];	
	}

	return sonuc;
}
//--------------------------------------------------------------------------------------------------
void String_CS_tad64(char *input_string, char *cs_tad64_string_out)
{
    U_16 instr_len, csum, i;

    instr_len = strlen(input_string);

    // calculate integer checksum
        csum = 0;
        for(i=0;i<instr_len;i++)
        {
            csum += input_string[i];
            csum &= 0x0FFF;             // limit csum to 12-bits
        }
    // convert it to 2-digit tad64 format
        Convert_U32_to_tad64_fixlenght(csum, cs_tad64_string_out, 2);    
}
//--------------------------------------------------------------------------------------------------
U_16 String_u16CS_tad64(char *in_str)
{
    U_16 instr_len, csum, i;

    instr_len = strlen(in_str);

    // calculate integer checksum
        csum = 0;
        for(i=0;i<instr_len;i++)
        {
            csum += in_str[i];
            csum &= 0x0FFF;             // limit csum to 12-bits
        }
    // return U_16 value
        return csum;    
}
//--------------------------------------------------------------------------------------------------
TBOOL Check_str_CS_tad64(char *in_str)      // check last 2 chars of input string for TAD64 checksum
{
    char incsum_str[3];
    U_16 instr_len, csum, csumin, i;

    instr_len = strlen(in_str);

    // calculate integer checksum
        csum = 0;
        for(i=0;i<(instr_len-2);i++)
        {
            csum += in_str[i];
            csum &= 0x0FFF;             // limit csum to 12-bits
        }

    // find input string csum
        incsum_str[0] = in_str[instr_len-2];
        incsum_str[1] = in_str[instr_len-1];
        incsum_str[2] = NULL;
        csumin = convert_tad64_to_U32(incsum_str);

    // compare the results
        if(csum == csumin)      return TRUE;
        else                    return FALSE;
}
//--------------------------------------------------------------------------------------------------
void Convert_string_to_visible_chars(char *instr)
{
	U_16 i, instrlen;

	instrlen = strlen(instr);

	for(i=0;i<instrlen;i++)
	{
        //if(instr[i] < ' ')		instr[i] = '_';
        if(instr[i] == _DC1)	instr[i] = '+';
        if(instr[i] == _DC2)	instr[i] = '-';
        if(instr[i] == _DC3)	instr[i] = '*';
        if(instr[i] == _DC4)	instr[i] = '/';
        if(instr[i] == _ACK)	instr[i] = '<';
        if(instr[i] == _NAK)	instr[i] = '>';
        if(instr[i] == _SOH)	instr[i] = '{';
        if(instr[i] == _EOT)	instr[i] = '}';
        if(instr[i] == _STX)	instr[i] = '[';
        if(instr[i] == _ETX)	instr[i] = ']';
        if(instr[i] == _GS)		instr[i] = '_';
	}
}
//--------------------------------------------------------------------------------------------------
void String_leftshift(char *instr, U_16 shiftno)
{
    U_16 instr_len, i, k;

    instr_len = strlen(instr);

    for(i=0;i<shiftno;i++)
    {
        for(k=0;k<instr_len;k++)
        {
            instr[k] = instr[k+1];
        }
    }
}
//--------------------------------------------------------------------------------------------------
void String_to_substring(char *instr, char *outstr, S_16 start_index, S_16 end_index)
{
    U_16 i;
    
    if((start_index < 0) || (end_index < 0))
    {
        outstr[0] = NULL;
        return;
    }
    
    outstr[0] = NULL;
    for(i=start_index; i<=end_index; i++)
    {
        outstr[i-start_index] = instr[i];
    }
    outstr[i-start_index] = NULL;
}
//--------------------------------------------------------------------------------------------------
S_16 String_contains_substring(char *instr, char *substr, U_16 start_index)
{
    U_16 i,k;
    
    for(i=start_index; i<(strlen(instr)); i++)
    {
        TBOOL match_ok;
        match_ok = TRUE;
        for(k=0; k<strlen(substr); k++)
        {
            if(substr[k] != instr[i+k])
            {
                match_ok = FALSE;
                break;
            }
        }
        
        if(match_ok == TRUE)
        {
            return i;
        }
    }

    return (-1);   
}
//--------------------------------------------------------------------------------------------------
U_64  Getnumber_from_string (char *metin, U_16 sequence)
{
	U_16 metinlen,i;
	U_64 sayi;
	U_16 aktifsira,siranin_basi, siranin_sonu;
	char sayimetin[15];

	// metin uzunlugu 1000'den buyukse sinirla
		metinlen = strlen(metin);
		metinlen = Value_limit(metinlen, 1000);

	// metni tara, istenilen kismin bas ve sonunu bul
		aktifsira = 0;
		siranin_basi = 0;
		siranin_sonu = 0;
        
		for(i=0; i<=metinlen; i++)
		{
			if((metin[i]==',')||(metin[i]==';')||(metin[i]=='*')||(metin[i]==':')||(metin[i]==_CR)||(metin[i]==_LF)||(metin[i]==0))
			{
			 	if(aktifsira >= sequence)
				{
				 	siranin_sonu = i;
					break;
				}
				else
				{
				 	aktifsira++;
					siranin_basi = i;
					siranin_sonu = i;
				}
			}
		}

	// bulunan kismi metine aktar.
		if(siranin_basi != 0)	siranin_basi++;
		if(siranin_basi >= siranin_sonu)	return 0;
		siranin_sonu = Value_limit(siranin_sonu,siranin_basi+10);
        
		for(i=siranin_basi; i<siranin_sonu; i++)
		{
		 	sayimetin[i-siranin_basi] = metin[i];
		}
		sayimetin[i-siranin_basi] = 0; // string sonlandir

	// metni sayisa cevri ve dondur
		sayi = Linkas_atoi_U64(sayimetin);

	return sayi;
}
//--------------------------------------------------------------------------------------------------
U_64 Getnumber_from_string_atgiven_position (char *metin, U_16 position)
{
	U_16 metinlen,i;
	U_64 sayi;
	U_16 siranin_basi, siranin_sonu;
	char sayimetin[15];

	// metin uzunlugu 1000'den buyukse sinirla
		metinlen = strlen(metin);
		metinlen = Value_limit(metinlen, 1000);

	// metni tara, istenilen kismin bas ve sonunu bul
		siranin_basi=position;
		siranin_sonu=position;
		for(i=position;i<metinlen;i++)
		{
			if((metin[i]==',')||(metin[i]==';')||(metin[i]=='*')||(metin[i]==_CR)||(metin[i]==_LF)||(metin[i]==0))
			{
				 	siranin_sonu = i;
					break;
			}
		}

	// bulunan kismi metine aktar.
		if(siranin_basi >= siranin_sonu)	return 0;
		siranin_sonu = Value_limit(siranin_sonu, siranin_basi+10);
		for(i=siranin_basi; i<siranin_sonu; i++)
		{
		 	sayimetin[i-siranin_basi] = metin[i];
		}
		sayimetin[i-siranin_basi] = 0; // string sonlandir

	// metni sayiya cevri ve dondur
		sayi = Linkas_atoi_U64(sayimetin);

	return sayi;
}
//--------------------------------------------------------------------------------------------------
TBOOL Getstring_from_string(char *inputmetin, U_16 sequence, char *outputmetin)
{
	U_16 metinlen,i;
	U_16 aktifsira,siranin_basi, siranin_sonu;
	char sayimetin[300];

	// metin uzunlugu 1000'den buyukse sinirla
		metinlen = strlen(inputmetin);
		metinlen = Value_limit(metinlen,1000);

	// metni tara, istenilen kismin bas ve sonunu bul
		aktifsira=0;
		siranin_basi=0;
		siranin_sonu=0;
		for(i=0; i<=metinlen; i++)
		{
			if((inputmetin[i]==',')||(inputmetin[i]==';')||(inputmetin[i]=='*')||(inputmetin[i]==':')||(inputmetin[i]==_CR)||(inputmetin[i]==_LF)||(inputmetin[i]==0))
			{
			 	if(aktifsira >= sequence)
				{
				 	siranin_sonu = i;
					break;
				}
				else
				{
				 	aktifsira++;
					siranin_basi = i;
					siranin_sonu = i;
				}
			}
		}

	// bulunan kismi metine aktar.
		if(siranin_basi != 0)	siranin_basi++;
		if(siranin_basi >= siranin_sonu)
        {	
            outputmetin[0] = NULL;
            return FALSE;
        }
        
		siranin_sonu = Value_limit(siranin_sonu,siranin_basi+300);
		for(i=siranin_basi;i<siranin_sonu;i++)
		{
		 	sayimetin[i-siranin_basi] = inputmetin[i];
		}
		sayimetin[i-siranin_basi] = 0; // string sonlandir

	// metni dondur
		strcpy(outputmetin, sayimetin);

	return TRUE;
}
//--------------------------------------------------------------------------------------------------
TBOOL Get_limitedstring_from_string(char *inputmetin, U_16 sequence, char *outputmetin, U_16 outmetin_len)
{
	U_16 metinlen,i;
	U_16 aktifsira,siranin_basi, siranin_sonu;
	char sayimetin[300];

	// metin uzunlugu 1000'den buyukse sinirla
		metinlen = strlen(inputmetin);
		metinlen = Value_limit(metinlen,1000);

	// metni tara, istenilen kismin bas ve sonunu bul
		aktifsira=0;
		siranin_basi=0;
		siranin_sonu=0;
		for(i=0; i <= metinlen; i++)
		{
			if((inputmetin[i]==',')||(inputmetin[i]==';')||(inputmetin[i]=='*')||(inputmetin[i]==':')||(inputmetin[i]==_CR)||(inputmetin[i]==_LF)||(inputmetin[i]==0))
			{
			 	if(aktifsira >= sequence)
				{
				 	siranin_sonu = i;
					break;
				}
				else
				{
				 	aktifsira++;
					siranin_basi = i;
					siranin_sonu = i;
				}
			}
		}

	// bulunan kismi metine aktar.
		if(siranin_basi != 0)	siranin_basi++;
		if(siranin_basi >= siranin_sonu)
        {	
            outputmetin[0] = NULL;
            return FALSE;
        }
        
		siranin_sonu = Value_limit(siranin_sonu,siranin_basi+300);
		for(i=siranin_basi;i<siranin_sonu;i++)
		{
            if((i-siranin_basi) < outmetin_len)
            {
		 	    sayimetin[i-siranin_basi] = inputmetin[i];
            }
		}
        // string sonlandir
        if((i-siranin_basi) < outmetin_len) sayimetin[i-siranin_basi] = 0;
        else                                sayimetin[outmetin_len-1] = 0;

	// metni dondur
		strcpy(outputmetin, sayimetin);

	return TRUE;
}
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
U_64 Value_limit(U_64 input_num, U_64 limit_val)
{
	if(input_num < limit_val)   return input_num;
	else                        return limit_val;
}
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------









