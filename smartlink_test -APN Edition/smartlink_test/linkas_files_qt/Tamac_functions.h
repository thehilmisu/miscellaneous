#ifndef __TAMAC_FUNCTIONS_H
#define __TAMAC_FUNCTIONS_H
//------------------------------------------------------------------------------
// Description: TAMAC (TADEL MACHINE COMMUNICATION) FUNCTIONS
//
// SANAL ELEKTRONIK LTD STI
// ATB IS MERKEZI NO:19 MACUNKOY
// ANKARA-TURKIYE
//
// (c) Copyright 2009-2010, SANAL ELEKTRONIK LTD STI
//
// Programmers: TOC
//
// VERSION : 1.2
//
// Version History:
//    V1.0: 2009-12-13
//          1. Initial release for hexa project
//    V1.1: 2010-03-14
//          2. Update for changes in constants and utilities files
//    V1.2: 2010-08-08
//          3. Tamac_get_par_no() function renamed as Tamac_get_param_no()
//          4. Tamac_get_intvalue() function is added
//          5. Tamac_get_candata_id() function is added
//
//------------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include "Linkas_constants.h"
#include "Linkas_datatypes_Qt.h"
#include "Linkas_utilities.h"


//------------------------------------------------------------------------------
//	FUNCTION PROTOTYPES
//------------------------------------------------------------------------------
extern S_8  Tamac_str_make_from_str(char packet_id, U_32 param_no, char *param_str, char *tamac_return_str);
// construct tamac string by parameter number and parameter string
// return string in tamac_return_str

extern S_8  Tamac_str_make_from_int(char packet_id, U_32 param_no, U_32 param_val, char *tamac_return_str);
// construct tamac string by parameter number and parameter integer value
// return string in tamac_return_str

extern S_8	 Tamac_str_make_candata(U_32 can_id, U_8 datalen, U_32 can_datah, U_32 can_datal, char *tamac_return_str);
// construct taman canbus data from parameters
// return string in tamac_return_str

extern U_16  Tamac_csum_find(char *in_str);
// calculate tamac checksum of input string. all characters are included to calculation

extern S_8  Tamac_check_csum(char *in_tamac_str);
// check the checksum of input tamac string, return TRUE of FALSE

extern S_8  Tamac_get_packet_id(char *in_tamac_str);
// return packet id of input tamac string

extern U_32 Tamac_get_param_no(char *in_tamac_str);
// return parameter no of input tamac string

extern U_32 Tamac_get_intvalue(char *in_tamac_str);
// return integer value of input tamac string

extern S_8  Tamac_get_group_alldata(char *in_tamac_str, char *return_group_data);
// return group data of input tamac string

extern S_8  Tamac_get_group_data(char *in_tamac_str, U_8 sequence, char *return_group_data);
// return group data of input tamac string at specified sequence (starting with 0)

extern U_32 Tamac_get_candata_ID(char *in_tamac_str);
// return CAN_ID from input tamac string

extern U_8 Tamac_get_candata_datalen(char *in_tamac_str);
// return can data len from input tamac string

extern U_64 Tamac_get_candata_val(char *in_tamac_str);
// return can data value from input tamac string

extern U_32 Tamac_get_candata_int_val(U_64 datain, U_8 start_byte, U_8 byte_len);
// return integer data of candata, at given start position (starting with 0) and given byte lenght (start with 1)

extern U_8 Tamac_get_candata_bit_val(U_64 datain, U_8 byte_pos, U_8 bit_pos, U_8 bit_len);
// return bit data of candata, at given byte position (starting with 0), given bit position (starting with 0 lsb bit) and given bit lenght (start with 1)

//------------------------------------------------------------------------------
//	CONSTANT & DEFINITIONS
//------------------------------------------------------------------------------
#define MAX_RX_LEN		300

#define _SET_STR		'#'
#define _SET_INT		'@'
#define _GET_VAL		'*'
#define _PUT_INTVAL		'$'
#define _PUT_STRVAL		'<'
#define _CAN_DAT		'C'





//------------------------------------------------------------------------------
//	VARIABLES
//------------------------------------------------------------------------------






#endif
