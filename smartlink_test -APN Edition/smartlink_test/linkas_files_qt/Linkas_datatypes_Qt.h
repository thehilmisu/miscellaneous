#ifndef __LINKAS_DATATYPES_CBUILDER_H
#define __LINKAS_DATATYPES_CBUILDER_H
//====================================================================
// LINKAS ELEKTRONIK LTD STI
// GERSAN SAN.SIT. 2310/1 SK NO:70
// 06370 BATIKENT ANKARA-TURKIYE
// www.linkas.com.tr
//
// (c) Copyright LINKAS ELEKTRONIK LTD STI
//
// Programmers  : TOC,
// Date         : 2009/12/13
// Description  : Linkas data types for Borland C++ Builder
//
// VERSION : 1.1
//
// Version History:
//    V1.0: 2009-12-13
//         - Initial release
//    V1.1: 2010-01-24, TOC
//          1. BOOL definition is changed with TBOOL
//          
//====================================================================



typedef unsigned char      TBOOL; // Logical data type (TRUE or FALSE)

typedef	unsigned char      U_8;  // Unsigned  8 bit value

typedef	signed char        S_8;  // Signed    8 bit value

typedef	unsigned short     U_16; // Unsigned 16 bit value

typedef	signed short       S_16; // Signed   16 bit value

typedef	unsigned int       U_32; // Unsigned 32 bit value

typedef	signed int         S_32; // Signed   32 bit value

typedef	unsigned __int64   U_64; // Unsigned 64 bit value

typedef	signed __int64     S_64; // Signed   64 bit value

typedef	float              FP_32;// 32 bit floating point

typedef	double             FP_64;// 64 bit floating point


#endif 
