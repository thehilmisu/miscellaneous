//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Author: TOLGA
// Copyright (c) 2010 S.E. LTD. All rights reserved.
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

#include "tamac_functions.h"


//------------------------------------------------------------------------------
//	FUNCTION PROTOTYPES
//------------------------------------------------------------------------------
S_8  Tamac_str_make_from_str(char packet_id, U_32 param_no, char *param_str, char *tamac_return_str);
S_8  Tamac_str_make_from_int(char packet_id, U_32 param_no, U_32 param_val, char *tamac_return_str);
S_8	 Tamac_str_make_candata(U_32 can_id, U_8 datalen, U_32 can_datah, U_32 can_datal, char *tamac_return_str);
S_8  Tamac_append_csum_eot(char *in_out_str);
U_16 Tamac_csum_find(char *in_str);
S_8  Tamac_check_csum(char *in_str);
S_8  Tamac_get_packet_id(char *in_str);
U_32 Tamac_get_param_no(char *in_str);
U_32 Tamac_get_intvalue(char *in_str);
S_8  Tamac_get_group_alldata(char *in_str, char *return_group_data);
S_8  Tamac_get_group_data(char *in_str,  U_8 sequence, char *return_group_data);
U_32 Tamac_get_candata_ID(char *in_tamac_str);
U_8  Tamac_get_candata_datalen(char *in_tamac_str);
U_64 Tamac_get_candata_val(char *in_tamac_str);
U_32 Tamac_get_candata_int_val(U_64 datain, U_8 start_byte, U_8 byte_len);
U_8  Tamac_get_candata_bit_val(U_64 datain, U_8 byte_pos, U_8 bit_pos, U_8 bit_len);
// return bit data of candata, at given byte position (starting with 0), given bit position (starting with 0 lsb bit) and given bit lenght (start with 1)

//------------------------------------------------------------------------------
//	CONSTANT & DEFINITIONS
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
//	EXTERN VARIABLES
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//	LOCAL VARIABLES
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
//	FUNCTIONS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
S_8 Tamac_str_make_from_str (char packet_id, U_32 param_no, char *param_str, char *tamac_return_str)
{
        char parno_t64[5];

        // find tad64 values
                Convert_U32_to_tad64(param_no, parno_t64);

        // find tamac string
                sprintf(tamac_return_str,"%c%c%s%c%s%c",_SOH,packet_id,parno_t64,_STX,param_str,_ETX);
                Tamac_append_csum_eot(tamac_return_str);

        return 0;
}
//------------------------------------------------------------------------------
S_8 Tamac_str_make_from_int (char packet_id, U_32 param_no, U_32 param_val, char *tamac_return_str)
{
        char parno_t64[5];
        char parval_t64[7];

        // find tad64 values
                Convert_U32_to_tad64(param_no, parno_t64);
                Convert_U32_to_tad64(param_val, parval_t64);

        // find tamac string
                sprintf(tamac_return_str,"%c%c%s%c%s%c",_SOH,packet_id,parno_t64,_STX,parval_t64,_ETX);
                Tamac_append_csum_eot(tamac_return_str);

        return 0;
}
//------------------------------------------------------------------------------
S_8	 Tamac_str_make_candata(U_32 can_id, U_8 datalen, U_32 can_datah, U_32 can_datal, char *tamac_return_str)
{
        U_16 i;

        sprintf(tamac_return_str,"%c%c%X%c%d%c%08X%08X%c",_SOH,_CAN_DAT,can_id,_STX,datalen,_GS,can_datah,can_datal,_ETX);

        Tamac_append_csum_eot(tamac_return_str);
        return 0;
}
//------------------------------------------------------------------------------
S_8 Tamac_append_csum_eot (char *in_out_str)
{
        U_16 csum;
        char temp_str[300];
        char csum_t64[4];

        csum = Tamac_csum_find(in_out_str);
        Convert_U32_to_tad64_fixlenght(csum, csum_t64, 2);

        sprintf(temp_str,"%s%s%c",in_out_str,csum_t64,_EOT);
        strcpy(in_out_str, temp_str);

        return 0;
}
//------------------------------------------------------------------------------
U_16  Tamac_csum_find (char *in_str)
{
        U_16 csum;
        U_16 str_len, i;

        csum = 0;
        str_len = strlen(in_str);
        for(i=0;i<str_len;i++)
        {
                csum = (csum + in_str[i]) & 0x0FFF;
        }

        return csum;
}
//------------------------------------------------------------------------------
S_8 Tamac_check_csum (char *in_str)
{
        U_16 i,end_pos;
        U_16 csum;
        char csum_t64[4];

        // find eot position
        for(i=0; i<MAX_RX_LEN; i++)											// find end of string
        {
                if(in_str[i] == _EOT)
                {
                        end_pos = i;
                        break;
                }
        }

        if(end_pos < 4)
        {
                return FAIL;
        }

        // find checksum
        csum = 0;
        for(i=0; i<(end_pos-2); i++)										// find csum of first part
        {
                csum = (csum + in_str[i]) & 0x0FFF;
        }
        Convert_U32_to_tad64_fixlenght(csum, csum_t64, 2);

        // compare csums
        if((csum_t64[0] != in_str[end_pos-2])||(csum_t64[1] != in_str[end_pos-1]))
        {
                return FAIL;
        }

        return SUCCESS;
}
//------------------------------------------------------------------------------
S_8 Tamac_get_packet_id (char *in_str)
{
        return in_str[1];
}
//------------------------------------------------------------------------------
U_32 Tamac_get_param_no (char *in_str)
{
        U_32 par_no,i;
        char char_par_no[10];

        char_par_no[0] = NULL;

        for(i=2; i<11; i++)
        {
                if(in_str[i] == _STX)
                {
                        break;
                }
                else
                {
                        char_par_no[i-2] = in_str[i];
                }
        }

        char_par_no[i-2] = NULL;
        par_no = convert_tad64_to_U32(char_par_no);

        return par_no;
}
//------------------------------------------------------------------------------
U_32 Tamac_get_intvalue(char *in_str)
{
        char param_strval[10];

        Tamac_get_group_alldata(in_str, param_strval);
        return (convert_tad64_to_U32(param_strval));
}
//------------------------------------------------------------------------------
S_8 Tamac_get_group_alldata (char *in_str, char *return_group_data)
{
        U_16 group_start_pos,i;

        return_group_data[0] = NULL;

        for(i=0;i<MAX_RX_LEN;i++)
        {
                if(in_str[i] == _STX)
                {
                        group_start_pos = i+1;
                        break;
                }
        }
        for(i=group_start_pos;i<MAX_RX_LEN;i++)
        {
                if((in_str[i] == _ETX)||(in_str[i] == _EOT))
                {
                        break;
                }
                else
                {
                        return_group_data[i-group_start_pos] = in_str[i];
                }
        }
        return_group_data[i-group_start_pos] = NULL;

        return SUCCESS;
}
//------------------------------------------------------------------------------
S_8 Tamac_get_group_data (char *in_str,  U_8 sequence, char *return_group_data)
{
        U_16 group_start_pos, dongubasla, i, group_seq;

        group_seq = 0;
        return_group_data[0] = NULL;

        for(i=0;i<MAX_RX_LEN;i++)
        {
                if(in_str[i] == _STX)
                {
                        group_start_pos = i+1;
                        break;
                }
        }
        dongubasla = group_start_pos;
        for(i=dongubasla;i<MAX_RX_LEN;i++)
        {
                if((in_str[i] == _ETX)||(in_str[i] == _EOT))
                {
                        break;
                }
                else if(in_str[i] == _GS)
                {
                        group_seq++;
                        if(group_seq == sequence)
                        {
                                group_start_pos = i+1;
                        }
                        else if(group_seq > sequence)
                        {
                                break;
                        }
                        else
                        {
                        }
                }
                else
                {
                        if(group_seq == sequence)
                        {
                                return_group_data[i-group_start_pos] = in_str[i];
                                return_group_data[i-group_start_pos+1] = NULL;
                        }
                }
        }

        return SUCCESS;
}
//------------------------------------------------------------------------------
U_32 Tamac_get_candata_ID(char *in_tamac_str)
{
        U_32 par_no,i;
        char char_par_no[10];

        char_par_no[0] = NULL;

        for(i=2; i<11; i++)
        {
                if(in_tamac_str[i] == _STX)
                {
                        break;
                }
                else
                {
                        char_par_no[i-2] = in_tamac_str[i];
                }
        }

        char_par_no[i-2] = NULL;
        par_no = hatoi32(char_par_no);

        return par_no;
}
//------------------------------------------------------------------------------
U_8 Tamac_get_candata_datalen(char *in_tamac_str)
{
        char can_strlen[4];

        Tamac_get_group_data(in_tamac_str, 0, can_strlen);
        //return Tadeca_atoi_U32(can_strlen);
        return 0;
}
//------------------------------------------------------------------------------
U_64 Tamac_get_candata_val(char *in_tamac_str)
{
        char can_strval[20];

        Tamac_get_group_data(in_tamac_str, 1, can_strval);
        return hatoi64(can_strval);
}
//------------------------------------------------------------------------------
U_32 Tamac_get_candata_int_val(U_64 datain, U_8 start_byte, U_8 byte_len)
{
        U_8  shiftcount;
        U_32 dataout;

        // convert string to U_64
                //datain = hatoi64(candata);

        // rightshift to get start-byte
                shiftcount = start_byte << 3;
                datain = datain >> shiftcount;

        // filter the bytes of byte_len
                if(byte_len == 1)		dataout = datain & 0xFF;
                else if(byte_len == 2)	dataout = datain & 0xFFFF;
                else if(byte_len == 3)	dataout = datain & 0xFFFFFF;
                else if(byte_len == 4)	dataout = datain & 0xFFFFFFFF;
                else					dataout = 0;

        return dataout;
}
//------------------------------------------------------------------------------
U_8 Tamac_get_candata_bit_val (U_64 datain, U_8 byte_pos, U_8 bit_pos, U_8 bit_len)
{
        U_8  shiftcount;
        U_8  databyte;

        // convert string to U_64
                //datain = hatoi64(candata);

        // rightshift to get start-byte
                shiftcount = byte_pos << 3;
                datain = datain >> shiftcount;
                databyte = datain & 0xFF;

        // find bits
                databyte = databyte >> bit_pos;

                if(bit_len == 1)			databyte &= 0x01;
                else if(bit_len == 2)		databyte &= 0x03;
                else if(bit_len == 3)		databyte &= 0x07;
                else if(bit_len == 4)		databyte &= 0x0F;
                else if(bit_len == 5)		databyte &= 0x1F;
                else if(bit_len == 6)		databyte &= 0x3F;
                else if(bit_len == 7)		databyte &= 0x7F;
                else						databyte = 0;

        return databyte;
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------





