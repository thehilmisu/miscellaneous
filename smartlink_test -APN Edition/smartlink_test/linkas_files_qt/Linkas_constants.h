#ifndef __LINKAS_CONSTANTS_H
#define __LINKAS_CONSTANTS_H

//-----------------------------------------------------------------
// LINKAS ELEKTRONIK LTD STI
// GERSAN SAN.SIT. 2310/1 SK NO:70
// 06370 BATIKENT ANKARA-TURKIYE
// www.linkas.com.tr
//
// (c) Copyright LINKAS ELEKTRONIK LTD STI
//
// Programmers  : TOC, ARK
// Description  : LINKAS commonly used constants
//
// VERSION : 1.3
//
// Version History:
//    V1.0: 2009-09-01, Initial relase
//    V1.1: 2009-11-08
//        1. #ifndef preprocessors are added
//        2. MASK definitions are grouped as 32-bit, 16-bit and 8-bit
//    V1.2: 2009-12-29
//        3. NULL definition is added
//    V1.3: 2010-03-08
//        4. New ascii control characters are defined
//          
//-----------------------------------------------------------------

    #ifndef NULL
        #define NULL		'\0'
    #endif
    #ifndef SUCCESS
        #define SUCCESS		1
    #endif
    #ifndef FAIL
        #define FAIL		-1
    #endif

    #ifndef TRUE
        #define TRUE		1
        #define FALSE		0
    #endif

    #ifndef ON
        #define ON			1
        #define OFF			0
    #endif


    #define _SOH        0x01
    #define _STX        0x02
    #define _ETX        0x03
    #define _EOT        0x04
    #define _ACK        0x06
    #define _BACKSPACE  0x08
    #define _TAB        0x09
    #define _LF         0x0A
    #define _CR         0x0D
    #define _DLE        0x10
    #define _DC1        0x11
    #define _DC2        0x12
    #define _DC3        0x13
    #define _DC4        0x14
    #define _NAK        0x15
    #define _SYN        0x16
    #define _ESC        0x1B
    #define _GS         0x1D
    #define _DEL        0x7F




    #ifndef _BIT0
	#define	_BIT0		0x0001
	#define	_BIT1		0x0002
	#define	_BIT2		0x0004
	#define	_BIT3		0x0008
	#define	_BIT4		0x0010
	#define	_BIT5		0x0020
	#define	_BIT6		0x0040
	#define	_BIT7		0x0080
	#define	_BIT8		0x0100
	#define	_BIT9		0x0200
	#define	_BIT10		0x0400
	#define	_BIT11		0x0800
	#define	_BIT12		0x1000
	#define	_BIT13		0x2000
	#define	_BIT14		0x4000
	#define	_BIT15		0x8000
	#define	_BIT16		0x00010000
	#define	_BIT17		0x00020000
	#define	_BIT18		0x00040000
	#define	_BIT19		0x00080000
	#define	_BIT20		0x00100000
	#define	_BIT21		0x00200000
	#define	_BIT22		0x00400000
	#define	_BIT23		0x00800000
	#define	_BIT24		0x01000000
	#define	_BIT25		0x02000000
	#define	_BIT26		0x04000000
	#define	_BIT27		0x08000000
	#define	_BIT28		0x10000000
	#define	_BIT29		0x20000000
	#define	_BIT30		0x40000000
	#define	_BIT31		0x80000000
    #endif


    #ifndef _MASK32_0        // 32 bit mask definitions
	#define	_MASK32_0		0xFFFFFFFE
	#define	_MASK32_1		0xFFFFFFFD
	#define	_MASK32_2		0xFFFFFFFB
	#define	_MASK32_3		0xFFFFFFF7
	#define	_MASK32_4		0xFFFFFFEF
	#define	_MASK32_5		0xFFFFFFDF
	#define	_MASK32_6		0xFFFFFFBF
	#define	_MASK32_7		0xFFFFFF7F
	#define	_MASK32_8		0xFFFFFEFF
	#define	_MASK32_9		0xFFFFFDFF
	#define	_MASK32_10		0xFFFFFBFF
	#define	_MASK32_11		0xFFFFF7FF
	#define	_MASK32_12		0xFFFFEFFF
	#define	_MASK32_13		0xFFFFDFFF
	#define	_MASK32_14		0xFFFFBFFF
	#define	_MASK32_15		0xFFFF7FFF
	#define	_MASK32_16		0xFFFEFFFF
	#define	_MASK32_17		0xFFFDFFFF
	#define	_MASK32_18		0xFFFBFFFF
	#define	_MASK32_19		0xFFF7FFFF
	#define	_MASK32_20		0xFFEFFFFF
	#define	_MASK32_21		0xFFDFFFFF
	#define	_MASK32_22		0xFFBFFFFF
	#define	_MASK32_23		0xFF7FFFFF
	#define	_MASK32_24		0xFEFFFFFF
	#define	_MASK32_25		0xFDFFFFFF
	#define	_MASK32_26		0xFBFFFFFF
	#define	_MASK32_27		0xF7FFFFFF
	#define	_MASK32_28		0xEFFFFFFF
	#define	_MASK32_29		0xDFFFFFFF
	#define	_MASK32_30		0xBFFFFFFF
	#define	_MASK32_31		0x7FFFFFFF
    #endif

   #ifndef _MASK16_0        // 16 bit mask definitions
	#define	_MASK16_0		0xFFFE
	#define	_MASK16_1		0xFFFD
	#define	_MASK16_2		0xFFFB
	#define	_MASK16_3		0xFFF7
	#define	_MASK16_4		0xFFEF
	#define	_MASK16_5		0xFFDF
	#define	_MASK16_6		0xFFBF
	#define	_MASK16_7		0xFF7F
	#define	_MASK16_8		0xFEFF
	#define	_MASK16_9		0xFDFF
	#define	_MASK16_10		0xFBFF
	#define	_MASK16_11		0xF7FF
	#define	_MASK16_12		0xEFFF
	#define	_MASK16_13		0xDFFF
	#define	_MASK16_14		0xBFFF
	#define	_MASK16_15		0x7FFF
    #endif

  #ifndef _MASK8_0         // 8 bit mask definitions
	#define	_MASK8_0		0xFE
	#define	_MASK8_1		0xFD
	#define	_MASK8_2		0xFB
	#define	_MASK8_3		0xF7
	#define	_MASK8_4		0xEF
	#define	_MASK8_5		0xDF
	#define	_MASK8_6		0xBF
	#define	_MASK8_7		0x7F
    #endif


#endif 
