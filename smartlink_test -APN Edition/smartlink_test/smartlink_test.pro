#-------------------------------------------------
#
# Project created by QtCreator 2012-05-30T09:48:17
#
#-------------------------------------------------

QT       += core gui

TARGET = smartlink_test
TEMPLATE = app
win32 : DEFINES += _TTY_WIN_ QWT_DLL QT_DLL
unix : DEFINES += _TTY_POSIX_

SOURCES += main.cpp\
        smartlinktest.cpp\
        SerialPort/qextserialbase.cpp \
        SerialPort/qextserialport.cpp \
        SerialPort/LinkasSerialPortThread.cpp \
        linkas_files_qt/Tamac_functions.cpp \
        linkas_files_qt/Linkas_utilities.cpp \
        LinkasSerialPortLister/LinkasSerialPortLister.cpp


win32 : SOURCES += SerialPort/win_qextserialport.cpp

unix : SOURCES += SerialPort/posix_qextserialport.cpp\


HEADERS  += smartlinktest.h\
            SerialPort/qextserialbase.h \
            SerialPort/qextserialport.h \
            SerialPort/LinkasSerialPortThread.h\
            linkas_files_qt/Tamac_functions.h \
            linkas_files_qt/Linkas_utilities.h \
            linkas_files_qt/Linkas_datatypes_Qt.h \
            linkas_files_qt/Linkas_constants.h \
            LinkasSerialPortLister/LinkasSerialPortLister.h

win32 : HEADERS += src/SerialPort/win_qextserialport.h
win32 : LIBS += -lsetupapi
FORMS    += smartlinktest.ui \
            parameterForm.ui

RESOURCES += \
    resource.qrc
RC_FILE = icon.rc

