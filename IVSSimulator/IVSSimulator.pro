#-------------------------------------------------
#
# Project created by QtCreator 2019-09-30T14:29:32
#
#-------------------------------------------------

QT       += core gui network

TARGET = IVSSimulator
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    linkastcpclient.cpp

HEADERS  += mainwindow.h \
    definitions.h \
    linkastcpclient.h

FORMS    += mainwindow.ui

OTHER_FILES += \
    periodic.json
