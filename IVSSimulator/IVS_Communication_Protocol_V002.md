
##IVS Communication Protocol

###1- Purpose
    Purpose of this document is to define the communication protocol between IVS device and 
    server and define the messaging format for all message types.

###2- Scope

    This document's scope is basically the items below,
    -	Message formats from IVS device to server
    -	Message formats from server to IVS device
    -	Examples and format explanations of messages and it's features

###3- Message Types and Representation 
    - Introduction Message 'I'
    - Dashboard (instant/frequent) Message 'D'
    - Maintenance Message 'M'
    - Alarm/Warning Message 'A'
    - Periodic Message 'P'
    - Test (Debug) Message 'T' (parameter_read : ?)
    - I/O Message 'I/O'

###4- Priority Types
    1: very important
    .
    .
    5: less important

###5- Security
####Encryption/Decryption  
    For security purposes we are going to use HTTPs protocol and TCP server is going to handle the security 
    problems. But further research can be done and we can implement the encryption/decryption easily.
####Compression/Decompression
    For compression and decompression jsonc and jsonminifier will be studied.

    
###Message Formats

    Message format will be json for both device-to-server and server-to-device parts.
    The first 100 (can be more or less) parameters will be Linkas based specific parameters with 
    specific id's from 0 to 99 and the rest of the parameters will be added by the customer 
    and the id's of that parameters will be auto incremented.

```javascript
{
    "0":"A180706",   //parameter with id of 0 corresponds to id : string, represents unique device id
    "1":"",         //parameter with id of 1 corresponds to idmType : string, represents message type
    ...
    ...
    ...
    ...
}
```


```
After erasing the white spaces and unnecessary charachters from json formatted message, 
typical 20 parameter key/value pair message has less than 220 charachters, 

Example:
```

```javascript
{"0" :"55","1":"100","2" :"55","3":"100","4" :"55","5":"100","6" :"55","7":"100","8" :"55","9":"100","10" :"55","11":"100","12" :"55","13":"100","14" :"55","15":"100","16" :"55","17":"100","18" :"55","19":"100"}
```


#####Example Introduction Message
```javascript
{
    "0":"BA32123" //device id as string
    "1":"I"    //introduction message identifier as string
    "2" : 0      //packet no as integer
    "3" : 3        //priority identifier for the parser app on the server, can be decided later as integer
    "4" : Timestamp//timestamp from device when message creation as timestamp (bigint)
    "5" : "1.0.0.12", // ip address as string
    "6" : "3213546789545668", //imei number as string
    "7" : "21345464546432154",  //imsi number as string
    "8" : 100, // IVS device firmware version with a resolution of 100 as integer 
    "9" : 100, // IVS device XML version with a resolution of 100 as integer
    "10" : "05324764558", //sim card phone number as string
}
```
#####Example Periodic Message
```javascript
{
    "0":"BA32123" //device id as string
    "1":"P"    //introduction message identifier as string
    "2" : 0      //packet no as integer
    "3" : 3        //priority identifier for the parser app on the server, can be decided later as integer
    "4" : Timestamp//timestamp from device when message creation as timestamp (bigint)
    "11" : "1.0.0.12", // 11th parameter and value
    "12" : "3213546789545668", //12th parameter and value
    ....
    ....
}
```
#####Example Alarm/Warning Message
    Alarm/Warning messages sent individually with their corresponding unique id's starting from 200
    and occurence count.
```javascript
{
    "0":"BA32123" //device id as string
    "1":"A"    //introduction message identifier as string
    "2" : 0      //packet no as integer
    "3" : 3        //priority identifier for the parser app on the server, can be decided later as integer
    "4" : Timestamp//timestamp from device when message creation as timestamp (bigint)
    "15": 32.007 //latitude value as float
    "16": 45.676 //longitude value as float
    "201": 3 // id of 201 alarm occured 3 times

    ....
    ....
}
```

#####Example parameter read from device

######request

    For example, we want to read parameters which id's 1 and 24 respectively,
    we just set their values to a question mark "?" as strings. 

```javascript
{
    "0":"BA32123" //device id as string
    "1":"T"    //introduction message identifier as string
    "2" : 0      //packet no as integer
    "3" : 3        //priority identifier for the parser app on the server, can be decided later as integer
    "4" : Timestamp//timestamp from device when message creation as timestamp (bigint)
    "50": "?" // request the 50th parameter value
    "43": "?" // request the 43rd parameter value
}
```
######response
    In response of what we requested, we get the corresponding parameters of key/value pairs just like
    periodic message.
```javascript
{
    "0":"BA32123" //device id as string
    "1":"T"    //introduction message identifier as string
    "2" : 0      //packet no as integer
    "3" : 3        //priority identifier for the parser app on the server, can be decided later as integer
    "4" : Timestamp//timestamp from device when message creation as timestamp (bigint)
    "50": "55" // response for the 50th parameter value
    "43": "125" // response for the 43rd parameter value
}
```
#####Example parameter set
######request
    For setting a parameter we can send just like the periodic message but we just modify the
    message identifier from "P" to "T" which is test/debug message identifier.
```javascript
{
    "0":"BA32123" //device id as string
    "1":"T"    //introduction message identifier as string
    "2" : 0      //packet no as integer
    "3" : 3        //priority identifier for the parser app on the server, can be decided later as integer
    "4" : Timestamp//timestamp from device when message creation as timestamp (bigint)
    "50": "55" // value to be setted for the 50th parameter
    "43": "125" //value to be setted for the 43rd parameter
}
```
######response
    Parameter set message response will be like the periodic message but with the test/debug message
    identifier.
```javascript
{
    "0":"BA32123" //device id as string
    "1":"T"    //introduction message identifier as string
    "2" : 0      //packet no as integer
    "3" : 3        //priority identifier for the parser app on the server, can be decided later as integer
    "4" : Timestamp//timestamp from device when message creation as timestamp (bigint)
    "50": "55" // value for the 50th parameter
    "43": "125" //value for the 43rd parameter
}
```

#####Document History
| version | authors          | changes         | reviewers |
|---------|------------------|-----------------|-----------|
| v0.0.1  | mert,habip,hakan | initial release |   tolga, hakan, habip, mert        |
| v0.0.2  | hakan            | json types has changed to decrease the message charachter count  |     ----      |