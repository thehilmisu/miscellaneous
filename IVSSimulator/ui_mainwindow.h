/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Fri Nov 29 11:30:51 2019
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QPushButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QLabel *label;
    QLineEdit *txtServer;
    QLabel *label_2;
    QLineEdit *txtPort;
    QPushButton *btnConnect;
    QPushButton *btnStartSimulation;
    QPushButton *btnGenerateAlarmMessage;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(400, 191);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 10, 46, 13));
        txtServer = new QLineEdit(centralWidget);
        txtServer->setObjectName(QString::fromUtf8("txtServer"));
        txtServer->setGeometry(QRect(43, 6, 181, 20));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(230, 10, 46, 13));
        txtPort = new QLineEdit(centralWidget);
        txtPort->setObjectName(QString::fromUtf8("txtPort"));
        txtPort->setGeometry(QRect(267, 6, 121, 20));
        btnConnect = new QPushButton(centralWidget);
        btnConnect->setObjectName(QString::fromUtf8("btnConnect"));
        btnConnect->setGeometry(QRect(10, 40, 381, 31));
        btnStartSimulation = new QPushButton(centralWidget);
        btnStartSimulation->setObjectName(QString::fromUtf8("btnStartSimulation"));
        btnStartSimulation->setGeometry(QRect(10, 90, 381, 31));
        btnGenerateAlarmMessage = new QPushButton(centralWidget);
        btnGenerateAlarmMessage->setObjectName(QString::fromUtf8("btnGenerateAlarmMessage"));
        btnGenerateAlarmMessage->setGeometry(QRect(10, 140, 381, 31));
        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "TCP Simulator", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MainWindow", "Host", 0, QApplication::UnicodeUTF8));
        txtServer->setText(QApplication::translate("MainWindow", "88.255.152.242", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MainWindow", "Port", 0, QApplication::UnicodeUTF8));
        txtPort->setText(QApplication::translate("MainWindow", "4439", 0, QApplication::UnicodeUTF8));
        btnConnect->setText(QApplication::translate("MainWindow", "Connect", 0, QApplication::UnicodeUTF8));
        btnStartSimulation->setText(QApplication::translate("MainWindow", "Start Simulation", 0, QApplication::UnicodeUTF8));
        btnGenerateAlarmMessage->setText(QApplication::translate("MainWindow", "Generate Alarm Message", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
