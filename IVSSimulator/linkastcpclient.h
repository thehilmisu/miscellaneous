#ifndef LINKASTCPCLIENT_H
#define LINKASTCPCLIENT_H

#include <QThread>
#include <QObject>
#include <QMutex>
#include <QTcpSocket>
#include <QHostAddress>
#include <QDebug>
#include <QDateTime>
#include "definitions.h"

class LinkasTCPClient : public QThread
{
public:
    LinkasTCPClient(QHostAddress addr,int p,QString dID);
    void generateAlarmMessage();
    QTcpSocket* getSocket()
    {
        return this->socket;
    }

public slots:
    void connect2server();
    void socketConnected();
    void socketDisconnected();

private:
    QTcpSocket *socket;
    QHostAddress address;
    QString deviceID;
    QList<QString> periodic_list;
    QMutex mute;
    QString generateIntroductionMessage(QString dID);
    QString generatePeriodicMessage(QString dID);
    int port;
    bool connectionStatus;
    int state;

protected:
    void run();
};

#endif // LINKASTCPCLIENT_H
