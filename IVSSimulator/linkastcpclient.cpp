#include "linkastcpclient.h"


LinkasTCPClient::LinkasTCPClient(QHostAddress addr,int p,QString dID)
{
    address = addr;
    port = p;
    connectionStatus = false;
    deviceID = dID;
    state = CONNECTION_STATE;

    socket = new QTcpSocket();
    connect(socket,SIGNAL(connected()),this,SLOT(socketConnected()));
    connect(socket,SIGNAL(disconnected()),this,SLOT(socketDisconnected()));


    periodic_list.append(PERIODIC_MESSAGE_EXC);
    periodic_list.append(PERIODIC_MESSAGE_EXC_2);
    periodic_list.append(MESSAGE_EXAMPLE_1);
    periodic_list.append(MESSAGE_EXAMPLE_2);
    periodic_list.append(MESSAGE_EXAMPLE_3);
    periodic_list.append(MESSAGE_EXAMPLE_4);
    periodic_list.append(MESSAGE_EXAMPLE_5);
    periodic_list.append(MESSAGE_EXAMPLE_6);
    periodic_list.append(MESSAGE_EXAMPLE_7);
    periodic_list.append(MESSAGE_EXAMPLE_8);
    periodic_list.append(MESSAGE_EXAMPLE_9);
    periodic_list.append(MESSAGE_EXAMPLE_10);

    this->start();

}
QString LinkasTCPClient::generateIntroductionMessage(QString dID)
{
    uint t = QDateTime::currentMSecsSinceEpoch();
    QString timestamp = QString::number(t);
    QString temp = "{\"0\":\""+dID+"\",\"1\":\"I\",\"2\":0,\"3\":3,\"4\":"+timestamp+
                   ",\"5\":\"1.0.0.12\",\"6\":\"3213546789545668\",\"7\":\"21345464546432154\","+
                   "\"8\":100,\"9\":100,\"10\":\"05324764558\""+
                   "}";
    return temp;
}
QString LinkasTCPClient::generatePeriodicMessage(QString dID)
{
    uint t = QDateTime::currentDateTime().toTime_t();
    QString timestamp = QString::number(t);
    static int dummy = 1;
    QString temp = "{\"0\":\""+dID+"\",\"1\":\"P\",\"2\":0,\"3\":3,\"4\":"+timestamp+
                   ",\""+QString::number(dummy+100)+"\":\""+QString::number(dummy)+"\",\"106\":\""+
                   QString::number(dummy+5)+
                   "\",\""+QString::number(dummy+102)+"\":\"21345464546432154\","+
                   "\""+QString::number(dummy+108)+"\":"+QString::number(dummy)+
                   ",\""+QString::number(dummy+125)+"\":100,\""+QString::number(dummy+265)+
                   "\":\""+QString::number(dummy+200)+"\""+
                   "}";
    dummy++;
    if(dummy > 100)
        dummy = 1;

    return temp;
}

void LinkasTCPClient::generateAlarmMessage()
{
    uint t = QDateTime::currentMSecsSinceEpoch();
    QString timestamp = QString::number(t);
    QString temp = "{\"0\":\""+deviceID+"\",\"1\":\"A\",\"2\":0,\"3\":3,\"4\":"+timestamp+
                   ",\"15\":32.007,\"16\":45.676,\"201\":3"+
                   "}";

    socket->write(temp.toUtf8().data()); //write the data itself

    //qDebug() << tmp.toUtf8().data();

    if(!socket->waitForBytesWritten())
    {

    }
}

void LinkasTCPClient::socketConnected()
{
    //mute.lock();
    qDebug() << "conn";
    connectionStatus = true;
    //mute.unlock();
}

void LinkasTCPClient::socketDisconnected()
{
    //mute.lock();
    connectionStatus = false;
    //mute.unlock();
}

void LinkasTCPClient::run()
{
    forever
    {
        //try, connect, send
        if(state == CONNECTION_STATE)
        {
            socket->connectToHost(address,port);
            if(socket->isOpen())
            {
                //mute.lock();
                state = INTRODUCTION_STATE;
                //mute.unlock();
                this->sleep(5);
            }
        }
        else if(state == INTRODUCTION_STATE)
        {
            //mute.lock();
            QString tmp = generateIntroductionMessage(deviceID);
            //qDebug() << tmp;
            //socket->write((const char*)&size); //write size of data
            socket->write(tmp.toUtf8().data()); //write the data itself
            if(!socket->waitForBytesWritten())
            {
                state = CONNECTION_STATE;
            }
            else
            {
                state = PERIODIC_STATE;
            }

            //mute.unlock();
        }
        else if(state == PERIODIC_STATE)
        {

            QString tmp = generatePeriodicMessage(deviceID);

            socket->write(tmp.toUtf8().data()); //write the data itself

            qDebug() << tmp.toUtf8().data();

            if(!socket->waitForBytesWritten())
            {
                state = CONNECTION_STATE;
            }

        }

        //qDebug() << deviceID << state;
        this->sleep(10);
    }
}
